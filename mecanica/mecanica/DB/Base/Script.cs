﻿//-- MySQL Workbench Forward Engineering

//SET @OLD_UNIQUE_CHECKS =@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
//SET @OLD_FOREIGN_KEY_CHECKS =@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
//SET @OLD_SQL_MODE =@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
//
//-- -----------------------------------------------------
//-- Schema mydb
//-- -----------------------------------------------------
//-- -----------------------------------------------------
//-- Schema dbmecanica
//-- -----------------------------------------------------
//
//-- -----------------------------------------------------
//-- Schema dbmecanica
//-- -----------------------------------------------------
//
//CREATE SCHEMA IF NOT EXISTS `dbmecanica` DEFAULT CHARACTER SET utf8 ;
//USE `dbmecanica` ;
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_categoria`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_categoria` (
//  `id_categoria` INT(11) NOT NULL,
//  `nm_categoria` VARCHAR(25) NOT NULL,
//  PRIMARY KEY (`id_categoria`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_cliente`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_cliente` (
//  `id_cliente` INT(11) NOT NULL AUTO_INCREMENT,
//  `nm_cliente` VARCHAR(25) NOT NULL,
//  `nm_sobrenome` VARCHAR(35) NOT NULL,
//  `dt_nascimento` DATETIME NOT NULL,
//  `ds_genero` VARCHAR(15) NOT NULL,
//  `ds_RG` VARCHAR(15) NOT NULL,
//  ds_cep varchar(15) NOT NULL,
//  `ds_telefone` VARCHAR(15) NOT NULL,
//  `ds_email` VARCHAR(55) NOT NULL,
//  PRIMARY KEY (`id_cliente`))
//ENGINE = InnoDB
//AUTO_INCREMENT = 16
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_fornecedor`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_fornecedor` (
//  `id_fornecedor` INT(11) NOT NULL AUTO_INCREMENT,
//  `nm_fornecedor` VARCHAR(35) NOT NULL,
//  `ds_telefone` VARCHAR(35) NOT NULL,
//  `ds_email` VARCHAR(55) NOT NULL,
//  `ds_cep` VARCHAR(15) NOT NULL,
//  PRIMARY KEY (`id_fornecedor`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_produto`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_produto` (
//  `id_produto` INT(11) NOT NULL AUTO_INCREMENT,
//  `nm_produto` VARCHAR(45) NOT NULL,
//  `vl_preço` DECIMAL(8,2) NOT NULL,
//  `id_categoria` INT(11) NOT NULL,
//  `ds_marca` VARCHAR(25) NOT NULL,
//  PRIMARY KEY (`id_produto`),
//  INDEX `tb_produto_fk1` (`id_categoria` ASC) ,
//  CONSTRAINT `tb_produto_fk1`
//    FOREIGN KEY (`id_categoria`)
//    REFERENCES `dbmecanica`.`tb_categoria` (`id_categoria`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_estoque`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_estoque` (
//  `id_estoque` INT(11) NOT NULL AUTO_INCREMENT,
//  `id_produto` INT(11) NOT NULL,
//  `ds_quantidade` VARCHAR(10) NOT NULL,
//  `tb_fornecedor_id_fornecedor` INT(11) NOT NULL,
//  PRIMARY KEY (`id_estoque`),
//  INDEX `tb_estoque_fk0` (`id_produto` ASC) ,
//  INDEX `fk_tb_estoque_tb_fornecedor1_idx` (`tb_fornecedor_id_fornecedor` ASC) ,
//  CONSTRAINT `fk_tb_estoque_tb_fornecedor1`
//    FOREIGN KEY (`tb_fornecedor_id_fornecedor`)
//    REFERENCES `dbmecanica`.`tb_fornecedor` (`id_fornecedor`),
//  CONSTRAINT `tb_estoque_fk0`
//    FOREIGN KEY (`id_produto`)
//    REFERENCES `dbmecanica`.`tb_produto` (`id_produto`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_funcionario`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_funcionario` (
//  `id_funcionario` INT(11) NOT NULL AUTO_INCREMENT,
//  `nm_funcionario` VARCHAR(35) NOT NULL,
//  `nm_sobrenome` VARCHAR(35) NOT NULL,
//  `ds_cpf` VARCHAR(15) NOT NULL,
//  `ds_rg` VARCHAR(15) NOT NULL,
//  `ds_cep` VARCHAR(15) NOT NULL,
//  `ds_complemento` VARCHAR(25) NOT NULL,
//  `dt_nascimento` DATETIME NOT NULL,
//  `ds_login` VARCHAR(25) NOT NULL,
//  `ds_senha` VARCHAR(25) NOT NULL,
//  `ds_telefone` VARCHAR(15) NOT NULL,
//  `nr_salario_bruto` DOUBLE NOT NULL,
// 
//  `ds_email` VARCHAR(55) NOT NULL,
//  `bt_permissaoadm` boolean ,
//  `bt_permissaovendedor` boolean,
//  bt_permissaomecanico boolean,
//  bt_planodesaude boolean,
//  PRIMARY KEY (`id_funcionario`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_folhadepagamento`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_folhadepagamento` (
//  `id_folhadepagamento` INT(11) NOT NULL AUTO_INCREMENT,
//  `nr_horasextras` INT(3) NULL,
//  `bt_valetransporte` TINYINT NULL,
//  `vl_inss` DECIMAL NULL,
//  `vl_ir` DECIMAL NULL,
//  `vl_fgts` DECIMAL NULL,
//  `vl_valetransporte` DECIMAL NULL,
//  `vl_salarioliquido` DECIMAL NULL,
//  `id_funcionario` INT(11) NOT NULL,
//  PRIMARY KEY (`id_folhadepagamento`),
//  INDEX `fk_tb_folhadepagamento_tb_funcionario1_idx` (`id_funcionario` ASC) ,
//  CONSTRAINT `fk_tb_folhadepagamento_tb_funcionario1`
//    FOREIGN KEY (`id_funcionario`)
//    REFERENCES `dbmecanica`.`tb_funcionario` (`id_funcionario`)
//    ON DELETE NO ACTION
//    ON UPDATE NO ACTION)
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_parceria`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_parceria` (
//  `id_parceria` INT(11) NOT NULL AUTO_INCREMENT,
//  `nm_parceria` VARCHAR(35) NOT NULL,
//  `ds_telefone` VARCHAR(15) NOT NULL,
//  `ds_email` VARCHAR(55) NOT NULL,
//  `ds_cep` VARCHAR(15) NOT NULL,
//  PRIMARY KEY (`id_parceria`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_pedido`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_pedido` (
//  `id_Pedido` INT(11) NOT NULL AUTO_INCREMENT,
//  `id_Funcionario` INT(11) NOT NULL,
//  `id_Cliente` INT(11) NOT NULL,
//  PRIMARY KEY (`id_Pedido`),
//  INDEX `tb_pedido_fk0` (`id_Funcionario` ASC) ,
//  INDEX `tb_pedido_fk1` (`id_Cliente` ASC) ,
//  CONSTRAINT `tb_pedido_fk0`
//    FOREIGN KEY (`id_Funcionario`)
//    REFERENCES `dbmecanica`.`tb_funcionario` (`id_funcionario`),
//  CONSTRAINT `tb_pedido_fk1`
//    FOREIGN KEY (`id_Cliente`)
//    REFERENCES `dbmecanica`.`tb_cliente` (`id_cliente`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//

//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_pedido_item`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_pedido_item` (
//  `id_pedido_item` INT(11) NOT NULL AUTO_INCREMENT,
//  `id_pedido` INT(11) NOT NULL,
//  PRIMARY KEY (`id_pedido_item`),
//  INDEX `tb_pedido_item_fk0` (`id_pedido` ASC) ,
//  CONSTRAINT `tb_pedido_item_fk0`
//    FOREIGN KEY (`id_pedido`)
//    REFERENCES `dbmecanica`.`tb_pedido` (`id_Pedido`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_serviço`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_serviço` (
//  `id_serviço` INT(11) NOT NULL AUTO_INCREMENT,
//  `nm_serviço` VARCHAR(35) NOT NULL,
//  `vl_preço` DECIMAL(10,0) NOT NULL,
//  `ds_servico` VARCHAR(50) NOT NULL,
//  PRIMARY KEY (`id_serviço`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//-- -----------------------------------------------------
//-- Table `dbmecanica`.`tb_pedido_serviço`
//-- -----------------------------------------------------
//CREATE TABLE IF NOT EXISTS `dbmecanica`.`tb_pedido_serviço` (
//  `id_pedido_serviço` INT(11) NOT NULL AUTO_INCREMENT,
//  `id_Pedido` INT(11) NOT NULL,
//  `id_serviço` INT(11) NOT NULL,
//  PRIMARY KEY (`id_pedido_serviço`),
//  INDEX `tb_pedido_serviço_fk0` (`id_Pedido` ASC) ,
//  INDEX `tb_pedido_serviço_fk1` (`id_serviço` ASC),
//  CONSTRAINT `tb_pedido_serviço_fk0`
//    FOREIGN KEY (`id_Pedido`)
//    REFERENCES `dbmecanica`.`tb_pedido` (`id_Pedido`),
//  CONSTRAINT `tb_pedido_serviço_fk1`
//    FOREIGN KEY (`id_serviço`)
//    REFERENCES `dbmecanica`.`tb_serviço` (`id_serviço`))
//ENGINE = InnoDB
//DEFAULT CHARACTER SET = utf8;
//
//
//SET SQL_MODE=@OLD_SQL_MODE;
//SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
//SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
//