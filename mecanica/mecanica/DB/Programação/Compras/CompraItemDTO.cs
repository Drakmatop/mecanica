﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Compras
{
    class CompraItemDTO
    {
        public int Id { get; set; }

        public int IdProduto { get; set; }

        public int IdCompra { get; set; }
    }
}
