﻿using mecanica.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Compras
{
    class CompraBusiness
    {
        CompraDatabase db = new CompraDatabase();

        public int Salvar(CompraDTO compra, List<ProdutoDTO> produtos)
        {
            if(compra.QtdItens == 0)
            {
                throw new ArgumentException("Nome não pode estar vazio", "Américas Mecânica");
            }

            if (compra.QtdItens > 200)
            {
                throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
            }

            if (compra.ValorCompra == 0)
            {
                throw new ArgumentException("Valor da compra é obrigatório", "Américas Mecânica");
            }

            if (compra.ValorCompra > 100000)
            {
                throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
            }

            if (compra.DataCompra > DateTime.Now)
            {
                throw new ArgumentException("Data muito avançada", "Américas Mecânica");
            }
            
            foreach (ProdutoDTO dto in produtos)
            {
                if (dto.Nome == string.Empty)
                {
                    throw new ArgumentException("Nome não pode estar vazio", "Américas Mecânica");
                }

                if (dto.Nome.Length > 45)
                {
                    throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
                }

                if (dto.PreçoCompra == 0)
                {
                    throw new ArgumentException("Preço é obrigatório", "Américas Mecânica");
                }

                if (dto.PreçoCompra > 100000)
                {
                    throw new ArgumentException("Preço do produto é muito caro", "Américas Mecânica");
                }


                if (dto.PrecoVenda == 0)
                {
                    throw new ArgumentException("Preço é obrigatório", "Américas Mecânica");
                }

                if (dto.PrecoVenda > 100000)
                {
                    throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
                }

                if (dto.Marca == string.Empty)
                {
                    throw new ArgumentException("Marca é obrigatória", "Américas Mecânica");
                }

                if (dto.Marca.Length > 25)
                {
                    throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
                }

                if (dto.IdCategoria == 0)
                {
                    throw new ArgumentException("Obrigatório escolher categoria", "Américas Mecânica");
                }
                             
                if (dto.IdFornecedor == 0)
                {
                    throw new ArgumentException("Obrigatório escolher fornecedor", "Américas Mecânica");
                }


            }
            
            int idCompra = db.Salvar(compra);

            CompraItemBusiness compraBusiness = new CompraItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                CompraItemDTO compraDTO = new CompraItemDTO();
                compraDTO.IdCompra = idCompra;
                compraDTO.IdProduto = item.Id;

                compraBusiness.Salvar(compraDTO);
            }
            return idCompra;
        }

        public List<VwConsultarCompra> Consultar(string produto)
        {
            return db.Consultar(produto);
        }

        public void Remover(int id)
        {
            CompraItemBusiness business = new CompraItemBusiness();
            business.Remover(id);
            db.Remover(id);
        }
        public List<VwConsultarCompra> ConsultarPorId(int id)
        {
            return db.ConsultarPorId(id);
        }
    }
}
