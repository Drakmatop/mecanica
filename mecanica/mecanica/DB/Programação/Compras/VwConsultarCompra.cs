﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Compras
{
    class VwConsultarCompra
    {
        public string NomeProduto { get; set; }

        public string NomeFornecedor { get; set; }

        public decimal ValorCompra { get; set; }

        public int QtdUnidades { get; set; }

        public DateTime DataCompra { get; set; }

        public int Id { get; set; }

        public int IdProduto { get; set; }
    }
}
