﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Funcionario
{
    class FuncionarioDTO
    {
        public int Id { get; set; }

        public string  Nome { get; set; }

        public  string Sobrenome { get; set; }

        public string Cpf { get; set; }

        public string RG { get; set; }

        public string Cep { get; set; }

        public string Complemento { get; set; }

        public DateTime Nascimento { get; set; }

        public string Login { get; set; }

        public string Senha { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public bool PermissaoADM { get; set; }

        public bool PermissaoVENDEDOR { get; set; }

        public bool PermissaoMecanico { get; set; }

        public bool PlanodeSaude { get; set; }

        public decimal SalarioBruto { get; set; }
    }
}
