﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Funcionario
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Sobrenome == string.Empty)
            {
                throw new ArgumentException("Sobrenome não pode estar vazio", "Américas Mecânica");
            }           
            if (dto.RG == string.Empty)
            {
                throw new ArgumentException("RG não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Cpf == string.Empty)
            {
                throw new ArgumentException("Cpf não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Complemento == string.Empty)
            {
                throw new ArgumentException("Complemento não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Email não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Login == string.Empty)
            {
                throw new ArgumentException("Login não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Senha == string.Empty)
            {
                throw new ArgumentException("Senha não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode estar vazio", "Américas Mecânica");
            }
            if (dto.SalarioBruto == 0)
            {
                throw new ArgumentException("SalarioBruto não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Cep == string.Empty)
            {
                throw new ArgumentException("Cep não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Nascimento > DateTime.Now)
            {
                throw new ArgumentException("Data muito avançada", "Américas Mecânica");
            }


            if (dto.Nome.Length > 35)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Sobrenome.Length > 35)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.RG.Length > 15)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Cpf.Length > 15)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Complemento.Length > 15)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Email.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Login.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Senha.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Telefone.Length > 15)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.SalarioBruto > 10000)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Cep.Length > 35)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Nascimento.Year < 1999)
            {
                throw new ArgumentException("Funcionario não tem idade para trabalhar", "Américas Mecânica");
            }
            
            return db.Salvar(dto);
        }
        
        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }



        public FuncionarioDTO Logar(string usuario, string senha)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(usuario, senha);
        }


        public List<FuncionarioDTO> Consultar(string Nome)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.ConsultarFuncionario(Nome);
        }
        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);
        }
        public FuncionarioDTO ConsultarLoginPeloUsuario(string usuario)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.ConsultarLoginPeloUsuario(usuario);
        }
        }
}
