﻿using mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Funcionario
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script =
                @"INSERT INTO tb_funcionario(nm_funcionario, nm_sobrenome, ds_cpf, ds_rg, ds_cep, ds_complemento, dt_nascimento, ds_login, ds_senha, ds_telefone, ds_email, bt_permissaoadm, bt_permissaovendedor, bt_permissaomecanico, bt_planodesaude, nr_salario_bruto)
                VALUES
                (@nm_funcionario, @nm_sobrenome, @ds_cpf, @ds_rg, @ds_cep, @ds_complemento, @dt_nascimento, @ds_login, @ds_senha, @ds_telefone, @ds_email, @bt_permissaoadm, @bt_permissaovendedor, @bt_permissaomecanico, @bt_planodesaude, @nr_salario_bruto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_login", dto.Login));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("bt_permissaoadm", dto.PermissaoADM));
            parms.Add(new MySqlParameter("bt_permissaovendedor", dto.PermissaoVENDEDOR));
            parms.Add(new MySqlParameter("bt_permissaomecanico", dto.PermissaoMecanico));
            parms.Add(new MySqlParameter("bt_planodesaude", dto.PlanodeSaude));
            parms.Add(new MySqlParameter("nr_salario_bruto", dto.SalarioBruto));





            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }



        public FuncionarioDTO Logar(string Usuário, string Senha)
        {
            string script = @"Select * FROM tb_funcionario WHERE ds_login = @ds_login
                        AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_login", Usuário));
            parms.Add(new MySqlParameter("ds_senha", Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;
            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.Sobrenome = reader.GetString("nm_sobrenome");
                funcionario.Cep = reader.GetString("ds_cep");
                funcionario.Telefone = reader.GetString("ds_telefone");
                funcionario.Nascimento = reader.GetDateTime("dt_nascimento");
                funcionario.Login = reader.GetString("ds_login");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.PermissaoADM = reader.GetBoolean("bt_permissaoadm");
                funcionario.PermissaoVENDEDOR = reader.GetBoolean("bt_permissaoVendedor");
                funcionario.PermissaoMecanico = reader.GetBoolean("bt_permissaoMecanico");
                funcionario.PlanodeSaude = reader.GetBoolean("bt_planodesaude");
                funcionario.SalarioBruto = reader.GetDecimal("nr_salario_bruto");

            }
            reader.Close();

            return funcionario;
        }
        public List<FuncionarioDTO> ConsultarLogin(string Nome)
        {
            string script = @" SELECT nm_funcionario,ds_login, ds_senha,
            bt_permissaoadm,bt_permissaovendedor, bt_permissaomecanico
            FROM tb_funcionario where
              nm_funcionario like @nm_funcionario ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> func = new List<FuncionarioDTO>(); ;

            while (reader.Read())
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.Login = reader.GetString("ds_login");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.PermissaoADM = reader.GetBoolean("bt_permissaoadm");
                funcionario.PermissaoVENDEDOR = reader.GetBoolean("bt_permissaoVendedor");
                funcionario.PermissaoMecanico = reader.GetBoolean("bt_permissaomecanico");


                func.Add(funcionario);
            }
            reader.Close();

            return func;

        }
        public FuncionarioDTO ConsultarLoginPeloUsuario(string usuario)
        {
            string script = @" SELECT nm_funcionario,ds_login, ds_senha,
            bt_permissaoadm,bt_permissaovendedor, ds_email
            FROM tb_funcionario where
              ds_login = @ds_login";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_login", usuario));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            FuncionarioDTO funcionario = null;
            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.Login = reader.GetString("ds_login");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.PermissaoADM = reader.GetBoolean("bt_permissaoadm");
                funcionario.PermissaoVENDEDOR = reader.GetBoolean("bt_permissaoVendedor");
                funcionario.Email = reader.GetString("ds_email");
            }
            reader.Close();

            return funcionario;

        }



        public List<FuncionarioDTO> ConsultarFuncionario(string Nome)
        {
            string script = @"SELECT * FROM tb_funcionario 
                WHERE nm_funcionario like @nm_funcionario ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lis = new List<FuncionarioDTO>();

            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.Sobrenome = reader.GetString("nm_sobrenome");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.RG = reader.GetString("ds_rg");
                dto.Email = reader.GetString("ds_email");
                dto.Cep = reader.GetString("ds_cep");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.Login = reader.GetString("ds_login");
                dto.Senha = reader.GetString("ds_senha");
                dto.PermissaoADM = reader.GetBoolean("bt_permissaoadm");
                dto.PermissaoVENDEDOR = reader.GetBoolean("bt_permissaovendedor");
                dto.PermissaoMecanico = reader.GetBoolean("bt_permissaomecanico");
                dto.PlanodeSaude = reader.GetBoolean("bt_planodesaude");
                dto.SalarioBruto = reader.GetDecimal("nr_salario_bruto");

                lis.Add(dto);
            }

            reader.Close();

            return lis;
        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lis = new List<FuncionarioDTO>();

            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.Sobrenome = reader.GetString("nm_sobrenome");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.RG = reader.GetString("ds_rg");
                dto.Email = reader.GetString("ds_email");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.Login = reader.GetString("ds_login");
                dto.Senha = reader.GetString("ds_senha");
                dto.PermissaoADM = reader.GetBoolean("bt_permissaoadm");
                dto.PermissaoVENDEDOR = reader.GetBoolean("bt_permissaovendedor");
                dto.PermissaoMecanico = reader.GetBoolean("bt_permissaomecanico");
                dto.PlanodeSaude = reader.GetBoolean("bt_planodesaude");
                dto.SalarioBruto = reader.GetDecimal("nr_salario_bruto");

                lis.Add(dto);
            }

            reader.Close();

            return lis;
        }

        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE tb_funcionario set 
                    nm_funcionario = @nm_funcionario,
                    nm_sobrenome = @nm_sobrenome,
                    ds_cpf= @ds_cpf, 
                    ds_rg = @ds_rg,
                    ds_cep = @ds_cep,
                    ds_complemento = @ds_complemento, 
                    dt_nascimento = @dt_nascimento,
                    ds_login = @ds_login,
                    ds_senha = @ds_senha ,
                    ds_telefone = @ds_telefone,
                    ds_email = @ds_email ,
                    bt_permissaoadm = @bt_permissaoadm, 
                    bt_permissaovendedor = @bt_permissaovendedor, 
                    bt_permissaomecanico = @bt_permissaomecanico,
                    bt_planodesaude = @bt_planodesaude ,
                    nr_salario_bruto = @nr_salario_bruto
                    WHERE id_funcionario = @id_funcionario;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_login", dto.Login));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("bt_permissaoadm", dto.PermissaoADM));
            parms.Add(new MySqlParameter("bt_permissaovendedor", dto.PermissaoVENDEDOR));
            parms.Add(new MySqlParameter("bt_permissaomecanico", dto.PermissaoMecanico));
            parms.Add(new MySqlParameter("bt_planodesaude", dto.PlanodeSaude));
            parms.Add(new MySqlParameter("nr_salario_bruto", dto.SalarioBruto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }





    }
}