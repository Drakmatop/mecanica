﻿using mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_produto, vl_precocompra, vl_precovenda, ds_marca, id_categoria, id_fornecedor) 
                                VALUES (  @nm_produto, @vl_precocompra, @vl_precovenda, @ds_marca, @id_categoria, @id_fornecedor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_precocompra", dto.PreçoCompra));
            parms.Add(new MySqlParameter("vl_precovenda", dto.PrecoVenda));
            parms.Add(new MySqlParameter("ds_marca", dto.Marca));
            parms.Add(new MySqlParameter("id_categoria", dto.IdCategoria));
            parms.Add(new MySqlParameter("id_fornecedor", dto.IdFornecedor));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public List<ProdutoDTO> Buscar(string Nome)
        {
            string script = @"SELECT * FROM tb_produto 
                WHERE nm_produto like @nm_produto ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lis = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.PreçoCompra = reader.GetDecimal("vl_precocompra");
                dto.PrecoVenda = reader.GetDecimal("vl_precovenda");
                dto.Marca = reader.GetString("ds_marca");
                dto.IdCategoria = reader.GetInt32("id_categoria");
                dto.IdFornecedor = reader.GetInt32("id_fornecedor");

                lis.Add(dto);

            }

            reader.Close();

            return lis;
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
