﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Produto
{
    class ProdutoDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public decimal PreçoCompra{ get; set; }

        public decimal PrecoVenda { get; set; }

        public string Marca { get; set; }

        public int IdFornecedor { get; set; }

        public int IdCategoria { get; set; }




    }
}
