﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Produto
{
    class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();
        public int Salvar(ProdutoDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome não pode estar vazio", "Américas Mecânica");
            }
            if (dto.PreçoCompra == 0)
            {
                throw new ArgumentException("Custo não pode estar vazio", "Américas Mecânica");
            }
            if (dto.PrecoVenda == 0)
            {
                throw new ArgumentException("Valor de venda não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Marca == string. Empty)
            {
                throw new ArgumentException("Marca não pode estar vazio" , "Américas Mecânica");
            }
            if (dto.IdCategoria == 0)
            {
                throw new ArgumentException("Categoria não pode estar vazio" , "Américas Mecânica");
            }

       
            if (dto.Nome.Length > 45)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Marca.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }

            return db.Salvar(dto);
        }
        public List<ProdutoDTO> Buscar(string Nome)
        {
            return
            db.Buscar(Nome);

        }
        public void Remover(int id)
        {
            db.Remover(id);
        }

    }
}
