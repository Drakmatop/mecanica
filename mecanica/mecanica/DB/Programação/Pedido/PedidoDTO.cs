﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Pedido
{
    class PedidoDTO
    {
        public int Id{ get; set; }

        public string FormaPagamento{ get; set; }

        public DateTime Data{ get; set; }



        public int FuncionarioId{ get; set; }

        public int ClienteId{ get; set; }


    }
}
