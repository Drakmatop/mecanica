﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Programação.PedidoServico
{
    class PedidoServicoBusiness
    {
        PedidoServicoDatabase db = new PedidoServicoDatabase();
        public int Salvar(PedidoServicoDTO dto)
        {
            
            if (dto.Idserviço == 0)
            {
                throw new ArgumentException("Serviço não reconhecido", "Américas Mecanica");
            }
            if (dto.Idpedido== 0)
            {
                throw new ArgumentException("Pedido não reconhecido", "Américas Mecânica");
            }
            return db.Salvar(dto);
        }
        public List<PedidoServicoDTO> Listar()
        {
            return
            db.Listar();

        }
        public void Remover(int id)
        {
            db.Remover(id);
        }
    }
}
