﻿using mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Programação.PedidoServico
{
    class PedidoServicoDatabase
    {
        public int Salvar(PedidoServicoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_serviço (id_pedido, id_servico) 
                                VALUES (@id_pedido, @id_servico)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", dto.Idpedido));
            parms.Add(new MySqlParameter("id_servico", dto.Idserviço));
           

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PedidoServicoDTO> Listar()
        {
            string script = @"SELECT * from tb_pedido_serviço";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoServicoDTO> lis = new List<PedidoServicoDTO>();

            while (reader.Read())
            {
                PedidoServicoDTO dto = new PedidoServicoDTO();
                dto.Id = reader.GetInt32("id_pedido_serviço");
                dto.Idpedido = reader.GetInt32("id_pedido");
                dto.Idserviço = reader.GetInt32("id_servico");

                lis.Add(dto);
            }
            reader.Close();

            return lis;
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedido_serviço WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
