﻿using Loja_de_roupas.DB.Compras;
using mecanica.DB.Produto;
using mecanica.DB.Programação.Pedido;
using mecanica.DB.Programação.PedidoServico;
using mecanica.DB.Serviço;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            if (pedido.FormaPagamento == string.Empty)
            {
                throw new ArgumentException("FormaPagamento é obrigatório", "Américas Mecânica");
            }
            if (pedido.Data > DateTime.Now)
            {
                throw new ArgumentException("Data é Maior do que hoje", "Américas Mecânica");
            }
            if (pedido.FuncionarioId == 0)
            {
                throw new ArgumentException(" Funcionario é obrigatório", "Américas Mecânica");
            }
            if (pedido.ClienteId == 0)
            {
                throw new ArgumentException(" Cliente é obrigatório", "Américas Mecânica");
            }

            if (pedido.FormaPagamento.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
           

           

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }
        public int SalvarServico(PedidoDTO pedido, List<ServiçoDTO> produtos)
        {
            if (pedido.FormaPagamento == string.Empty)
            {
                throw new ArgumentException(" FormaPagamento é obrigatório", "Lottus Store");
            }
            if (pedido.Data > DateTime.Now)
            {
                throw new ArgumentException(" Data é Maior do que hoje", "Lottus Store");
            }
            if (pedido.FuncionarioId == 0)
            {
                throw new ArgumentException(" Funcionario é obrigatório", "Lottus Store");
            }
            if (pedido.ClienteId == 0)
            {
                throw new ArgumentException(" Cliente é obrigatório", "Lottus Store");
            }


            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idpedido = pedidoDatabase.Salvar(pedido);

            PedidoServicoBusiness compraBusiness = new PedidoServicoBusiness();
            foreach (ServiçoDTO item in produtos)
            {
                PedidoServicoDTO compraDto = new PedidoServicoDTO();
                compraDto.Idpedido = idpedido;
                compraDto.Idserviço = item.Id;

                compraBusiness.Salvar(compraDto);
            }

            return idpedido;
        }
        public List<VwPedidoServicoConsultar> ConsultarServico(string cliente)
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.ConsultarServico(cliente);
        }
            public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }

        public void Remover(int id)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Remover(id);
        }

        public List<PedidoConsultarView> ConsultarPorId(int id)
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.ConsultarPorId(id);
        }

        }
}
