﻿using mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (id_cliente, nm_cliente, nm_sobrenome, dt_nascimento, ds_telefone, ds_email, ds_rg, ds_genero, ds_cep) 
                                VALUES ( @id_cliente, @nm_cliente, @nm_sobrenome, @dt_nascimento, @ds_telefone, @ds_email, @ds_rg, @ds_genero, @ds_cep)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.Id));
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_genero", dto.Genero));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ClienteDTO> Buscar(string Nome)
        {
            string script = @"SELECT nm_cliente,nm_sobrenome,ds_telefone,ds_email,ds_genero,dt_nascimento FROM tb_cliente 
                WHERE nm_cliente like @nm_cliente ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lis = new List<ClienteDTO>();

            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Nome = reader.GetString("nm_cliente");
                dto.Sobrenome = reader.GetString("nm_sobrenome");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Email = reader.GetString("ds_email");
                dto.Genero = reader.GetString("ds_genero");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");

                lis.Add(dto);

            }

            reader.Close();

            return lis;
        }

        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lis = new List<ClienteDTO>();

            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Sobrenome = reader.GetString("nm_sobrenome");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Email = reader.GetString("ds_email");
                dto.Genero = reader.GetString("ds_genero");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.RG = reader.GetString("ds_rg");
                dto.Cep = reader.GetString("ds_cep");

                lis.Add(dto);

            }

            reader.Close();

            return lis;
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE INTO tb_cliente
                            set 
                    nm_cliente = @nm_cliente,
                    nm_sobrenome = @nm_sobrenome, 
                    ds_rg = @ds_rg,            
                    ds_cep = @ds_cep ,
                    ds_email = @ds_email, 
                    ds_genero = @ds_genero ,
                    ds_telefone = @ds_telefone,
                    dt_nascimento = @dt_nascimento
                    WHERE id_cliente = @id_cliente";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.Id));
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_genero", dto.Genero));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
    }
}
