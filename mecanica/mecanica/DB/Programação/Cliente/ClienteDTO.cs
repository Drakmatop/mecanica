﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Cliente
{
    class ClienteDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Sobrenome { get; set; }

        public DateTime Nascimento { get; set; }

        public string RG { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public string Genero { get; set; }

        public string Cep { get; set; }
    }
}
