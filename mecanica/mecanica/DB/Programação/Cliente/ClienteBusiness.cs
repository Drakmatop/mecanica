﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Cliente
{
    class ClienteBusiness
    {
        ClienteDatabase db = new ClienteDatabase();
        public int Salvar(ClienteDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome não pode estar vazio", "Américas Mecânica");
            }

            if (dto.Nome.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres" , "Américas Mecânica");
            }

            if (dto.Sobrenome == string.Empty)
            {
                throw new ArgumentException("Sobrenome não pode estar vazio", "Américas Mecânica");
            }

            if (dto.Sobrenome.Length > 35)
            {
                throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
            }
                if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode estar vazio", "Américas Mecânica");
            }

            if (dto.Telefone.Length > 15)
            {
                throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
            }

            if (dto.RG == string.Empty)
            {
                throw new ArgumentException("RG não pode estar vazio", "Américas Mecânica");
            }

            if (dto.RG.Length > 15)
            {
                throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
            }

            if (dto.Cep == string.Empty)
            {
                throw new ArgumentException("Cep não pode estar vazio", "Américas Mecânica");
            }

            if (dto.Cep.Length > 15)
            {
                throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
            }


            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Email não pode estar vazio", "Américas Mecânica");
            }

            if (dto.Email.Length > 55)
            {
                throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
            }

            if (dto.Genero == string.Empty)
            {
                throw new ArgumentException("Genero não pode estar vazio", "Américas Mecânica");
            }

            if (dto.Genero.Length > 15)
            {
                throw new ArgumentException("Sobrenome contém muitos caracteres", "Américas Mecânica");
            }

            if (dto.Nascimento.Year < 1999)
            {
                throw new ArgumentException("Cliente muito novo para possuir cadastro", "Américas Mecânica");
            }



            return db.Salvar(dto);
        }
        public List<ClienteDTO> Buscar(string Nome)
        {
            return
            db.Buscar(Nome);
                       
        }
        public void Remover(int id)
        {
             db.Remover(id);
        }

        public List<ClienteDTO> Listar()
        {
            return db.Listar();
        }
    }
}
