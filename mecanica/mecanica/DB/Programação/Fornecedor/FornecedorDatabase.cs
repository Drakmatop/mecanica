﻿using mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Programação.Fornecedor
{
    class FornecedorDatabase
    {
        Database db = new Database();
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_fornecedor
                (nm_fornecedor, ds_telefone, ds_cep, ds_email)
                VALUES
                (@nm_fornecedor, @ds_telefone, @ds_cep, @ds_email)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Nome));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_email", dto.Email));

            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<FornecedorDTO> Buscar(string Nome)
        {

            string script = @"SELECT * FROM tb_fornecedor 
                WHERE nm_fornecedor like @nm_fornecedor ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lis = new List<FornecedorDTO>();

            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("id_fornecedor");
                dto.Nome = reader.GetString("nm_fornecedor");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Email = reader.GetString("ds_email");
                dto.Cep = reader.GetString("ds_cep");

                lis.Add(dto);

            }

            reader.Close();

            return lis;

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<FornecedorDTO> Listar()
        {

            string script = @"SELECT * FROM tb_fornecedor ";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lis = new List<FornecedorDTO>();

            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("id_fornecedor");
                dto.Nome = reader.GetString("nm_fornecedor");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Email = reader.GetString("ds_email");
                dto.Cep = reader.GetString("ds_cep");

                lis.Add(dto);

            }

            reader.Close();

            return lis;

        }



    }
}
