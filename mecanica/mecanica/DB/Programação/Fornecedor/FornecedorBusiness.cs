﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Programação.Fornecedor
{
    class FornecedorBusiness
    {
        FornecedorDatabase db = new FornecedorDatabase();
        public int Salvar(FornecedorDTO dto)
        {

            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Email não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Cep == string.Empty)
            {
                throw new ArgumentException("Cep não pode estar vazio", "Américas Mecânica");
            }


            if (dto.Nome.Length > 35)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Telefone.Length > 35)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Email.Length > 55)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Cep.Length > 15)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }

            return db.Salvar(dto);
        }
        public List<FornecedorDTO> Buscar(string Nome)
        {
            return db.Buscar(Nome);
        }
        public List<FornecedorDTO> Listar()
        {
            return db.Listar();
        }
        public void Remover(int id)
        {
            db.Remover(id);
        }


    }
}
