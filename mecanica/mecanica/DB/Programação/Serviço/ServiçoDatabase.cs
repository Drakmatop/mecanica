﻿using mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Serviço
{
    class ServiçoDatabase
    {
        public int Salvar(ServiçoDTO dto)
        {
            string script = @"INSERT INTO tb_servico (nm_servico, vl_preco, ds_servico) 
                                VALUES (@nm_servico, @vl_preco, @ds_servico)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            
            parms.Add(new MySqlParameter("nm_servico", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("ds_servico", dto.Descricao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ServiçoDTO> Buscar(string Nome)
        {

            string script = @"SELECT * FROM tb_servico 
                WHERE nm_servico like @nm_servico ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_servico", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ServiçoDTO> lis = new List<ServiçoDTO>();

            while (reader.Read())
            {
                ServiçoDTO dto = new ServiçoDTO();
                dto.Id = reader.GetInt32("id_servico");
                dto.Nome = reader.GetString("nm_servico");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Descricao = reader.GetString("ds_servico");
               
                lis.Add(dto);

            }

            reader.Close();

            return lis;

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_servico WHERE id_servico = @id_servico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_servico", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


    }
}
