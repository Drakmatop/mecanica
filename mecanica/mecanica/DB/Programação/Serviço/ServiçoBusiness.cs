﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Serviço
{
    class ServiçoBusiness
    {
        ServiçoDatabase db = new ServiçoDatabase();
        public int Salvar(ServiçoDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Preco == 0)
            {
                throw new ArgumentException("Preço não pode estar vazio", "Américas Mecânica");
            }
            if (dto.Descricao == string.Empty)
            {
                throw new ArgumentException("Descrição não pode estar vazio", "Américas Mecânica");
            }


            if (dto.Nome.Length > 35)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }
            if (dto.Descricao.Length > 100)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }


           

            return db.Salvar(dto);
        }
        public List<ServiçoDTO> Buscar(string Nome)
        {
            return
            db.Buscar(Nome);

        }
        public void Remover(int id)
        {
            db.Remover(id);
        }
    }
}
