﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Programação.Categoria
{
    class CategoriaBusiness
    {
        CategoriaDatabase db = new CategoriaDatabase();

        public int Salvar(CategoriaDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome não pode estar vazio", "Américas Mecânica");
            }

            if (dto.Nome.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres", "Américas Mecânica");
            }

            return db.Salvar(dto);
        }
        public List<CategoriaDTO> Buscar(string Nome)
        {
            return
            db.Consultar(Nome);

        }
        public List<CategoriaDTO> Listar()
        {
            return db.Listar();
        }
       
        public void Remover(int id)
        {
            db.Remover(id);
        }

    }
}
