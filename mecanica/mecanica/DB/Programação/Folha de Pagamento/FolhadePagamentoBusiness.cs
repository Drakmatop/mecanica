﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mecanica.DB.Programação.Folha_de_Pagamento
{
    class FolhadePagamentoBusiness
    {
        FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
        public int Salvar(FolhadePagamentoDTO dto)
        {
            if (dto.Bt_Valetransporte == true)
            {
                if (dto.vl_ValeTransporte == 0)
                {
                    throw new ArgumentException("Vale Transporte é obrigatório", "Américas Mecânica");
                }
            }
            return db.Salvar(dto);
        }
        public List<FolhadePagamentoDTO> Buscar(string NomeFuncionario)
        {
            return db.Buscar(NomeFuncionario);
        }
        public void Remover(int Id)
        {
            db.Remover(Id);
        }

    }
}
