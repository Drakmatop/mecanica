﻿using mecanica.DB.Funcionario;
using mecanica.DB.Programação.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Cadastrar
{
    public partial class FolhadePagamento : Form
    {
        public FolhadePagamento()
        {
            InitializeComponent();

            lblText.Visible = false;
            lblSalarioLiquido.Visible = false;

            lblPorcentagem.Visible = false;
            nudPorcentagem.Visible = false;
            lblHoraExtra.Visible = false;
            nudHorasExtras.Visible = false;
            CarregarCombo();

        }
        decimal ValordeDesconto;
        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> Funcionarios = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = Funcionarios;
                
        }
        CalculosFolhadePagamento folha = new CalculosFolhadePagamento();
        decimal VT = 0;
        decimal SalarioBruto = 0;
        decimal DSR;
        decimal INSS;
        decimal IR;
        decimal FGTS;
        decimal SalarioLiquido;

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {



                int ht = Convert.ToInt32(nudHorasTrabalhadas.Value);
                int he = Convert.ToInt32(nudHorasExtras.Value);
                decimal porcentagem = nudPorcentagem.Value;
                int diastrabalhados = folha.CalcularDiasTrabalhados(Convert.ToInt32(txtFaltasPriSemana.Text), Convert.ToInt32(txtFaltaSegSemana.Text), Convert.ToInt32(txtFaltaTerSemana.Text), Convert.ToInt32(txtFaltaQuarSemana.Text));
                int finaisdesemana = folha.CalcularFinaisdeSemanas(Convert.ToInt32(txtFaltasPriSemana.Text), Convert.ToInt32(txtFaltaSegSemana.Text), Convert.ToInt32(txtFaltaTerSemana.Text), Convert.ToInt32(txtFaltaQuarSemana.Text));
                int totaldefaltas = folha.CalcularTotalDeFaltas(Convert.ToInt32(txtFaltasPriSemana.Text), Convert.ToInt32(txtFaltaSegSemana.Text), Convert.ToInt32(txtFaltaTerSemana.Text), Convert.ToInt32(txtFaltaQuarSemana.Text));

                FuncionarioDTO funcionario = cboFuncionario.SelectedItem as FuncionarioDTO;
                SalarioBruto = funcionario.SalarioBruto;

                if (nudHorasExtras.Value > 0 && chkVT.Checked == true)
                {

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    decimal totalhorasextras = folha.CalcularValorHoraExtra(valorporhora, porcentagem, he);
                    DSR = folha.CalcularDSR(totalhorasextras, diastrabalhados, finaisdesemana);
                    decimal TotalSalarioBruto = SalarioBruto + totalhorasextras + DSR;



                    INSS = folha.CalcularINSS(SalarioBruto, TotalSalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(TotalSalarioBruto, INSS);
                    IR = folha.CalcularIR(TotalSalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(TotalSalarioBruto);
                    VT = SalarioBruto * 0.06m;

                    SalarioLiquido = SalarioBruto + totalhorasextras + DSR - INSS - ValordeDesconto - VT;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();

                }
                else if (nudHorasExtras.Value > 0 && chkVT.Checked == false)
                {
                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    decimal totalhorasextras = folha.CalcularValorHoraExtra(valorporhora, porcentagem, he);
                    DSR = folha.CalcularDSR(totalhorasextras, diastrabalhados, finaisdesemana);
                    decimal TotalSalarioBruto = SalarioBruto + totalhorasextras + DSR;



                    INSS = folha.CalcularINSS(SalarioBruto, TotalSalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(TotalSalarioBruto, INSS);

                    IR = folha.CalcularIR(TotalSalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(TotalSalarioBruto);


                    decimal SalarioLiquido = SalarioBruto + totalhorasextras + DSR - INSS - ValordeDesconto - VT;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                }
                else if (nudHorasExtras.Value == 0 && chkVT.Checked == true)
                {

                    decimal descontodosalariobruto = SalarioBruto / 30 * totaldefaltas;
                    SalarioBruto = SalarioBruto - descontodosalariobruto;

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);

                    INSS = folha.CalcularINSS(SalarioBruto, SalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(SalarioBruto, SalarioBruto);

                    IR = folha.CalcularIR(SalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(SalarioBruto);
                    VT = SalarioBruto * 0.06m;

                    SalarioLiquido = SalarioBruto - INSS - ValordeDesconto - VT;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                }

                else if (nudHorasExtras.Value == 0 && chkVT.Checked == false)
                {
                    decimal descontodosalariobruto = SalarioBruto / 30 * totaldefaltas;
                    SalarioBruto = SalarioBruto - descontodosalariobruto;

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);

                    INSS = folha.CalcularINSS(SalarioBruto, SalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(SalarioBruto, SalarioBruto);

                    IR = folha.CalcularIR(SalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(SalarioBruto);

                    SalarioLiquido = SalarioBruto - INSS - ValordeDesconto - VT;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
        }

        private void chkHoraExtra_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHoraExtra.Checked == true)
            {
                lblPorcentagem.Visible = true;
                nudPorcentagem.Visible = true;
                lblHoraExtra.Visible = true;
                nudHorasExtras.Visible = true;
            }

            if (chkHoraExtra.Checked == false)
            {
                lblPorcentagem.Visible = false;
                nudPorcentagem.Visible = false;
                lblHoraExtra.Visible = false;
                nudHorasExtras.Visible = false;
            }

        }

        

        private void chkVT_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FolhadePagamentoDTO dto = new FolhadePagamentoDTO();
                FuncionarioDTO funcionario = cboFuncionario.SelectedItem as FuncionarioDTO;

                dto.Bt_Valetransporte = chkVT.Checked;
                dto.FGTS = FGTS;
                dto.HorasExtras = Convert.ToInt32(nudHorasExtras.Value);
                dto.INSS = INSS;
                dto.IR = IR;
                dto.SalarioLiquido = SalarioLiquido;
                dto.vl_ValeTransporte = VT;
                dto.IdFuncionario = funcionario.Id;
                dto.Pagamento = DateTime.Now;

                FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Folha de pagamento salva com sucesso", "America Mecanica");

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
           
            
        }
    }
}
