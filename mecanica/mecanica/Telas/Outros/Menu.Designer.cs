﻿namespace mecanica.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.folhaDePagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoServiçoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.gastoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.emailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tweeterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tweetarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.meusTweetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tweetsGeraisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Menu;
            this.label1.Location = new System.Drawing.Point(55, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(923, 108);
            this.label1.TabIndex = 1;
            this.label1.Text = "América\'s  Mecânica";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clienteToolStripMenuItem,
            this.funcionarioToolStripMenuItem,
            this.folhaDePagamentoToolStripMenuItem,
            this.produtoToolStripMenuItem,
            this.pedidoToolStripMenuItem,
            this.comprasToolStripMenuItem,
            this.categoriaToolStripMenuItem,
            this.fornecedorToolStripMenuItem,
            this.pedidoToolStripMenuItem1,
            this.pedidoServiçoToolStripMenuItem,
            this.estoqueToolStripMenuItem,
            this.fluxoDeCaixaToolStripMenuItem,
            this.gastoToolStripMenuItem,
            this.emailsToolStripMenuItem,
            this.tweeterToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 166);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1093, 24);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // clienteToolStripMenuItem
            // 
            this.clienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem4,
            this.consultarToolStripMenuItem3});
            this.clienteToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
            this.clienteToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.clienteToolStripMenuItem.Text = "Cliente";
            // 
            // cadastrarToolStripMenuItem4
            // 
            this.cadastrarToolStripMenuItem4.Name = "cadastrarToolStripMenuItem4";
            this.cadastrarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem4.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem4.Click += new System.EventHandler(this.cadastrarToolStripMenuItem4_Click);
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem3.Text = "Consultar";
            this.consultarToolStripMenuItem3.Click += new System.EventHandler(this.consultarToolStripMenuItem3_Click);
            // 
            // funcionarioToolStripMenuItem
            // 
            this.funcionarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem3,
            this.consultarToolStripMenuItem2});
            this.funcionarioToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.funcionarioToolStripMenuItem.Name = "funcionarioToolStripMenuItem";
            this.funcionarioToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.funcionarioToolStripMenuItem.Text = "Funcionario";
            // 
            // cadastrarToolStripMenuItem3
            // 
            this.cadastrarToolStripMenuItem3.Name = "cadastrarToolStripMenuItem3";
            this.cadastrarToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem3.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem3.Click += new System.EventHandler(this.cadastrarToolStripMenuItem3_Click);
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem2.Text = "Consultar";
            this.consultarToolStripMenuItem2.Click += new System.EventHandler(this.consultarToolStripMenuItem2_Click);
            // 
            // folhaDePagamentoToolStripMenuItem
            // 
            this.folhaDePagamentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem2,
            this.consultarToolStripMenuItem1});
            this.folhaDePagamentoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.folhaDePagamentoToolStripMenuItem.Name = "folhaDePagamentoToolStripMenuItem";
            this.folhaDePagamentoToolStripMenuItem.Size = new System.Drawing.Size(128, 20);
            this.folhaDePagamentoToolStripMenuItem.Text = "Folha de Pagamento";
            // 
            // cadastrarToolStripMenuItem2
            // 
            this.cadastrarToolStripMenuItem2.Name = "cadastrarToolStripMenuItem2";
            this.cadastrarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem2.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem2.Click += new System.EventHandler(this.cadastrarToolStripMenuItem2_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // produtoToolStripMenuItem
            // 
            this.produtoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem1,
            this.consultarToolStripMenuItem});
            this.produtoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.produtoToolStripMenuItem.Name = "produtoToolStripMenuItem";
            this.produtoToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.produtoToolStripMenuItem.Text = "Produto";
            // 
            // cadastrarToolStripMenuItem1
            // 
            this.cadastrarToolStripMenuItem1.Name = "cadastrarToolStripMenuItem1";
            this.cadastrarToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem1.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem1.Click += new System.EventHandler(this.cadastrarToolStripMenuItem1_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // pedidoToolStripMenuItem
            // 
            this.pedidoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem,
            this.consultarToolStripMenuItem5});
            this.pedidoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.pedidoToolStripMenuItem.Name = "pedidoToolStripMenuItem";
            this.pedidoToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.pedidoToolStripMenuItem.Text = "Serviço";
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem.Click += new System.EventHandler(this.cadastrarToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem5
            // 
            this.consultarToolStripMenuItem5.Name = "consultarToolStripMenuItem5";
            this.consultarToolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem5.Text = "Consultar";
            this.consultarToolStripMenuItem5.Click += new System.EventHandler(this.consultarToolStripMenuItem5_Click);
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem5,
            this.consultarToolStripMenuItem4});
            this.comprasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.comprasToolStripMenuItem.Text = "Compras";
            // 
            // cadastrarToolStripMenuItem5
            // 
            this.cadastrarToolStripMenuItem5.Name = "cadastrarToolStripMenuItem5";
            this.cadastrarToolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem5.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem5.Click += new System.EventHandler(this.cadastrarToolStripMenuItem5_Click);
            // 
            // consultarToolStripMenuItem4
            // 
            this.consultarToolStripMenuItem4.Name = "consultarToolStripMenuItem4";
            this.consultarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem4.Text = "Consultar";
            this.consultarToolStripMenuItem4.Click += new System.EventHandler(this.consultarToolStripMenuItem4_Click);
            // 
            // categoriaToolStripMenuItem
            // 
            this.categoriaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem6,
            this.consultarToolStripMenuItem6});
            this.categoriaToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.categoriaToolStripMenuItem.Name = "categoriaToolStripMenuItem";
            this.categoriaToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.categoriaToolStripMenuItem.Text = "Categoria";
            // 
            // cadastrarToolStripMenuItem6
            // 
            this.cadastrarToolStripMenuItem6.Name = "cadastrarToolStripMenuItem6";
            this.cadastrarToolStripMenuItem6.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem6.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem6.Click += new System.EventHandler(this.cadastrarToolStripMenuItem6_Click);
            // 
            // consultarToolStripMenuItem6
            // 
            this.consultarToolStripMenuItem6.Name = "consultarToolStripMenuItem6";
            this.consultarToolStripMenuItem6.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem6.Text = "Consultar";
            this.consultarToolStripMenuItem6.Click += new System.EventHandler(this.consultarToolStripMenuItem6_Click);
            // 
            // fornecedorToolStripMenuItem
            // 
            this.fornecedorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem7,
            this.consultarToolStripMenuItem7});
            this.fornecedorToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.fornecedorToolStripMenuItem.Name = "fornecedorToolStripMenuItem";
            this.fornecedorToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.fornecedorToolStripMenuItem.Text = "Fornecedor";
            // 
            // cadastrarToolStripMenuItem7
            // 
            this.cadastrarToolStripMenuItem7.Name = "cadastrarToolStripMenuItem7";
            this.cadastrarToolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem7.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem7.Click += new System.EventHandler(this.cadastrarToolStripMenuItem7_Click);
            // 
            // consultarToolStripMenuItem7
            // 
            this.consultarToolStripMenuItem7.Name = "consultarToolStripMenuItem7";
            this.consultarToolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem7.Text = "Consultar";
            this.consultarToolStripMenuItem7.Click += new System.EventHandler(this.consultarToolStripMenuItem7_Click);
            // 
            // pedidoToolStripMenuItem1
            // 
            this.pedidoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem9,
            this.cadastrarToolStripMenuItem8});
            this.pedidoToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.pedidoToolStripMenuItem1.Name = "pedidoToolStripMenuItem1";
            this.pedidoToolStripMenuItem1.Size = new System.Drawing.Size(56, 20);
            this.pedidoToolStripMenuItem1.Text = "Pedido";
            // 
            // cadastrarToolStripMenuItem9
            // 
            this.cadastrarToolStripMenuItem9.Name = "cadastrarToolStripMenuItem9";
            this.cadastrarToolStripMenuItem9.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem9.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem9.Click += new System.EventHandler(this.cadastrarToolStripMenuItem9_Click);
            // 
            // cadastrarToolStripMenuItem8
            // 
            this.cadastrarToolStripMenuItem8.Name = "cadastrarToolStripMenuItem8";
            this.cadastrarToolStripMenuItem8.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem8.Text = "Consultar";
            this.cadastrarToolStripMenuItem8.Click += new System.EventHandler(this.cadastrarToolStripMenuItem8_Click);
            // 
            // pedidoServiçoToolStripMenuItem
            // 
            this.pedidoServiçoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem10,
            this.consultarToolStripMenuItem8});
            this.pedidoServiçoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.pedidoServiçoToolStripMenuItem.Name = "pedidoServiçoToolStripMenuItem";
            this.pedidoServiçoToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.pedidoServiçoToolStripMenuItem.Text = "Pedido Serviço";
            // 
            // cadastrarToolStripMenuItem10
            // 
            this.cadastrarToolStripMenuItem10.Name = "cadastrarToolStripMenuItem10";
            this.cadastrarToolStripMenuItem10.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem10.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem10.Click += new System.EventHandler(this.cadastrarToolStripMenuItem10_Click);
            // 
            // consultarToolStripMenuItem8
            // 
            this.consultarToolStripMenuItem8.Name = "consultarToolStripMenuItem8";
            this.consultarToolStripMenuItem8.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem8.Text = "Consultar";
            this.consultarToolStripMenuItem8.Click += new System.EventHandler(this.consultarToolStripMenuItem8_Click);
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem9});
            this.estoqueToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.estoqueToolStripMenuItem.Text = "Estoque";
            // 
            // consultarToolStripMenuItem9
            // 
            this.consultarToolStripMenuItem9.Name = "consultarToolStripMenuItem9";
            this.consultarToolStripMenuItem9.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem9.Text = "Consultar";
            this.consultarToolStripMenuItem9.Click += new System.EventHandler(this.consultarToolStripMenuItem9_Click);
            // 
            // fluxoDeCaixaToolStripMenuItem
            // 
            this.fluxoDeCaixaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem10});
            this.fluxoDeCaixaToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.fluxoDeCaixaToolStripMenuItem.Name = "fluxoDeCaixaToolStripMenuItem";
            this.fluxoDeCaixaToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.fluxoDeCaixaToolStripMenuItem.Text = "Fluxo de Caixa";
            // 
            // consultarToolStripMenuItem10
            // 
            this.consultarToolStripMenuItem10.Name = "consultarToolStripMenuItem10";
            this.consultarToolStripMenuItem10.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem10.Text = "Consultar";
            this.consultarToolStripMenuItem10.Click += new System.EventHandler(this.consultarToolStripMenuItem10_Click);
            // 
            // gastoToolStripMenuItem
            // 
            this.gastoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem12,
            this.consultarToolStripMenuItem11});
            this.gastoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.gastoToolStripMenuItem.Name = "gastoToolStripMenuItem";
            this.gastoToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.gastoToolStripMenuItem.Text = "Gastos";
            // 
            // cadastrarToolStripMenuItem12
            // 
            this.cadastrarToolStripMenuItem12.Name = "cadastrarToolStripMenuItem12";
            this.cadastrarToolStripMenuItem12.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem12.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem12.Click += new System.EventHandler(this.cadastrarToolStripMenuItem12_Click);
            // 
            // consultarToolStripMenuItem11
            // 
            this.consultarToolStripMenuItem11.Name = "consultarToolStripMenuItem11";
            this.consultarToolStripMenuItem11.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem11.Text = "Consultar";
            this.consultarToolStripMenuItem11.Click += new System.EventHandler(this.consultarToolStripMenuItem11_Click);
            // 
            // emailsToolStripMenuItem
            // 
            this.emailsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enviarToolStripMenuItem});
            this.emailsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.emailsToolStripMenuItem.Name = "emailsToolStripMenuItem";
            this.emailsToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.emailsToolStripMenuItem.Text = "Emails";
            // 
            // enviarToolStripMenuItem
            // 
            this.enviarToolStripMenuItem.Name = "enviarToolStripMenuItem";
            this.enviarToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.enviarToolStripMenuItem.Text = "Enviar";
            this.enviarToolStripMenuItem.Click += new System.EventHandler(this.enviarToolStripMenuItem_Click);
            // 
            // tweeterToolStripMenuItem
            // 
            this.tweeterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tweetarToolStripMenuItem,
            this.meusTweetsToolStripMenuItem,
            this.tweetsGeraisToolStripMenuItem});
            this.tweeterToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tweeterToolStripMenuItem.Name = "tweeterToolStripMenuItem";
            this.tweeterToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.tweeterToolStripMenuItem.Text = "tweeter";
            // 
            // tweetarToolStripMenuItem
            // 
            this.tweetarToolStripMenuItem.Name = "tweetarToolStripMenuItem";
            this.tweetarToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.tweetarToolStripMenuItem.Text = "Tweetar";
            // 
            // meusTweetsToolStripMenuItem
            // 
            this.meusTweetsToolStripMenuItem.Name = "meusTweetsToolStripMenuItem";
            this.meusTweetsToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.meusTweetsToolStripMenuItem.Text = "Meus Tweets";
            // 
            // tweetsGeraisToolStripMenuItem
            // 
            this.tweetsGeraisToolStripMenuItem.Name = "tweetsGeraisToolStripMenuItem";
            this.tweetsGeraisToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.tweetsGeraisToolStripMenuItem.Text = "Tweets Gerais";
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = global::mecanica.Properties.Resources.fundoMecanicaI;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1093, 190);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funcionarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folhaDePagamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem categoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem fornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem pedidoServiçoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem gastoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem emailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tweeterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tweetarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem meusTweetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tweetsGeraisToolStripMenuItem;
    }
}