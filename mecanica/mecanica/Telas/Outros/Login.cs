﻿using mecanica.DB.Funcionario;
using mecanica.DB.Programação.Funcionario;
using mecanica.Telas;
using Nsf._2018.Modulo2.DB.Filosofia.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void login_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO Funcionario = business.Logar(txtUsuario.Text, txtSenha.Text);
                if (Funcionario != null)
                {
                    UserSession.UsuarioLogado = Funcionario;
                    frmMenu tela = new frmMenu();
                    tela.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "American Mecânica");
            }
        }

        private void lblEnvio_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text == string.Empty)
            {
                MessageBox.Show("Preencha o campo de usuário para continuar");
            }
            else
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO login = business.ConsultarLoginPeloUsuario(txtUsuario.Text);
                if (login == null)
                {
                    MessageBox.Show("Este usuário não existe, digite o usuário corretamente");
                }
                else
                {

                    Email email = new Email();
                    email.Mensagem = "Olá " + login.Nome + @", foi solicitado um envio de e-mail contendo a sua senha, caso não seja você a ter solicitado, relate o ocorrido ao seu supervisor.
a sua senha é : " + login.Senha;
                    email.Para = login.Email;
                    email.Assunto = "Relembre sua senha";
                    email.Enviar();
                    MessageBox.Show("Uma mensagem foi enviada no seu e-mail contendo sua senha, verifique-o e tente fazer login novamente");
                }
            }
        }
    }
}
