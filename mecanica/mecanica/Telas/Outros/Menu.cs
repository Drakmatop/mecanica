﻿using mecanica.DB.Programação.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            ValidarLogin();
        }

        public void ValidarLogin()
        {
            try
            {
                if (UserSession.UsuarioLogado.PermissaoADM == false)
                {
                    cadastrarToolStripMenuItem.Enabled = false;
                    cadastrarToolStripMenuItem1.Enabled = false;
                    cadastrarToolStripMenuItem2.Enabled = false;
                    cadastrarToolStripMenuItem3.Enabled = false;
                    cadastrarToolStripMenuItem4.Enabled = false;
                    cadastrarToolStripMenuItem5.Enabled = false;

                }

            }
            catch (Exception)
            {

                MessageBox.Show("Ocorreu um erro, Logue novamente", "American mecânica");
                Application.Exit();
            }
        }

        private void btnCadastrarCliente_Click(object sender, EventArgs e)
        {
            CadastrarCliente tela = new CadastrarCliente();
            tela.Show();
        }

        private void btnConsultarCliente_Click(object sender, EventArgs e)
        {
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
        }

        private void btnFolhaPagamento_Click(object sender, EventArgs e)
        {
            Telas.Cadastrar.FolhadePagamento tela = new Cadastrar.FolhadePagamento();
            tela.Show();
        }

        private void btnCadastrarFuncionario_Click(object sender, EventArgs e)
        {
            CadastrarFuncionario tela = new CadastrarFuncionario();
            tela.Show();
        }

        private void btnConsultarFuncionario_Click(object sender, EventArgs e)
        {
            ConsultarFuncionario tela = new ConsultarFuncionario();
            tela.Show();
        }

        private void btnConsultarFolha_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarFolhadePagamento tela = new Consultar.ConsultarFolhadePagamento();
            tela.Show();
            
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.Cadastrar.CadastrarServico tela = new Cadastrar.CadastrarServico();
            tela.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Telas.Cadastrar.CadastrarCompra tela = new Cadastrar.CadastrarCompra();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Telas.Cadastrar.CadastrarProduto tela = new Cadastrar.CadastrarProduto();
            tela.Show();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarProduto tela = new Consultar.ConsultarProduto();
            tela.Show();
        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Telas.Consultar.Consultar_Compra tela = new Consultar.Consultar_Compra();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Telas.Cadastrar.FolhadePagamento tela = new Cadastrar.FolhadePagamento();
            tela.Show();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarFolhadePagamento tela = new Consultar.ConsultarFolhadePagamento();
            tela.Show();

        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            CadastrarFuncionario tela = new CadastrarFuncionario();
            tela.Show();

        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ConsultarFuncionario tela = new ConsultarFuncionario();
            tela.Show();

        }

        private void cadastrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            CadastrarCliente tela = new CadastrarCliente();
            tela.Show();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            Telas.Cadastrar.Cadastar_Cartegoria tela = new Cadastrar.Cadastar_Cartegoria();
            tela.Show();
        }

        private void consultarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            Telas.Consultar.Consultar_Categoria tela = new Consultar.Consultar_Categoria();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            CadastrarFornecedor tela = new CadastrarFornecedor();
            tela.Show();
        }

        private void consultarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarFornecedor tela = new Consultar.ConsultarFornecedor();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarPedido tela = new Consultar.ConsultarPedido();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            Pedido tela = new Pedido();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            Telas.Cadastrar.Pedido_Serviço tela = new Cadastrar.Pedido_Serviço();
            tela.Show();
        }

        private void consultarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Telas.Consultar.Consultar_Pedido_Serviço tela = new Consultar.Consultar_Pedido_Serviço();
            tela.Show();
        }

        private void consultarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarServico tela = new Consultar.ConsultarServico();
            tela.Show();
        }

        private void consultarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            EstoqueItem estoque = new EstoqueItem();
            estoque.Show();
        }

        private void consultarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarFluxodeCaixa tela = new Consultar.ConsultarFluxodeCaixa();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarGastos tela = new Consultar.ConsultarGastos();
            tela.Show();
        }

        private void consultarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            Telas.Consultar.ConsultarGastos tela = new Consultar.ConsultarGastos();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            Telas.Cadastrar.CadastrarGastos tela = new Cadastrar.CadastrarGastos();
            tela.Show();
        }

        private void enviarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.Outros.Enviar_Email tela = new Outros.Enviar_Email();
            tela.Show();
        }
    }
}
