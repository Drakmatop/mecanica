﻿namespace mecanica.Telas.Cadastrar
{
    partial class FolhadePagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.chkVT = new System.Windows.Forms.CheckBox();
            this.nudHorasExtras = new System.Windows.Forms.NumericUpDown();
            this.lblHoraExtra = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudHorasTrabalhadas = new System.Windows.Forms.NumericUpDown();
            this.lblPorcentagem = new System.Windows.Forms.Label();
            this.nudPorcentagem = new System.Windows.Forms.NumericUpDown();
            this.lblText = new System.Windows.Forms.Label();
            this.lblSalarioLiquido = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.chkHoraExtra = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFaltasPriSemana = new System.Windows.Forms.TextBox();
            this.txtFaltaSegSemana = new System.Windows.Forms.TextBox();
            this.txtFaltaQuarSemana = new System.Windows.Forms.TextBox();
            this.txtFaltaTerSemana = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasExtras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasTrabalhadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPorcentagem)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(134, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Funcionário :";
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(226, 45);
            this.cboFuncionario.Margin = new System.Windows.Forms.Padding(4);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(191, 24);
            this.cboFuncionario.TabIndex = 0;
            // 
            // chkVT
            // 
            this.chkVT.AutoSize = true;
            this.chkVT.BackColor = System.Drawing.Color.Transparent;
            this.chkVT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkVT.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.chkVT.Location = new System.Drawing.Point(159, 236);
            this.chkVT.Margin = new System.Windows.Forms.Padding(4);
            this.chkVT.Name = "chkVT";
            this.chkVT.Size = new System.Drawing.Size(115, 20);
            this.chkVT.TabIndex = 3;
            this.chkVT.Text = "Vale transporte";
            this.chkVT.UseVisualStyleBackColor = false;
            this.chkVT.CheckedChanged += new System.EventHandler(this.chkVT_CheckedChanged);
            // 
            // nudHorasExtras
            // 
            this.nudHorasExtras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudHorasExtras.Location = new System.Drawing.Point(226, 286);
            this.nudHorasExtras.Margin = new System.Windows.Forms.Padding(4);
            this.nudHorasExtras.Name = "nudHorasExtras";
            this.nudHorasExtras.Size = new System.Drawing.Size(191, 22);
            this.nudHorasExtras.TabIndex = 6;
            // 
            // lblHoraExtra
            // 
            this.lblHoraExtra.AutoSize = true;
            this.lblHoraExtra.BackColor = System.Drawing.Color.Transparent;
            this.lblHoraExtra.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblHoraExtra.Location = new System.Drawing.Point(127, 288);
            this.lblHoraExtra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHoraExtra.Name = "lblHoraExtra";
            this.lblHoraExtra.Size = new System.Drawing.Size(91, 16);
            this.lblHoraExtra.TabIndex = 5;
            this.lblHoraExtra.Text = "Horas Extras :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(86, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Horas Trabalhadas :";
            // 
            // nudHorasTrabalhadas
            // 
            this.nudHorasTrabalhadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudHorasTrabalhadas.Location = new System.Drawing.Point(226, 77);
            this.nudHorasTrabalhadas.Margin = new System.Windows.Forms.Padding(4);
            this.nudHorasTrabalhadas.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudHorasTrabalhadas.Name = "nudHorasTrabalhadas";
            this.nudHorasTrabalhadas.Size = new System.Drawing.Size(191, 22);
            this.nudHorasTrabalhadas.TabIndex = 1;
            // 
            // lblPorcentagem
            // 
            this.lblPorcentagem.AutoSize = true;
            this.lblPorcentagem.BackColor = System.Drawing.Color.Transparent;
            this.lblPorcentagem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblPorcentagem.Location = new System.Drawing.Point(123, 326);
            this.lblPorcentagem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPorcentagem.Name = "lblPorcentagem";
            this.lblPorcentagem.Size = new System.Drawing.Size(95, 16);
            this.lblPorcentagem.TabIndex = 9;
            this.lblPorcentagem.Text = "Porcentagem :";
            // 
            // nudPorcentagem
            // 
            this.nudPorcentagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudPorcentagem.DecimalPlaces = 2;
            this.nudPorcentagem.Location = new System.Drawing.Point(226, 324);
            this.nudPorcentagem.Margin = new System.Windows.Forms.Padding(4);
            this.nudPorcentagem.Name = "nudPorcentagem";
            this.nudPorcentagem.Size = new System.Drawing.Size(191, 22);
            this.nudPorcentagem.TabIndex = 7;
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.BackColor = System.Drawing.Color.Transparent;
            this.lblText.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblText.Location = new System.Drawing.Point(114, 366);
            this.lblText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(104, 16);
            this.lblText.TabIndex = 10;
            this.lblText.Text = "Salário Líquido :";
            // 
            // lblSalarioLiquido
            // 
            this.lblSalarioLiquido.AutoSize = true;
            this.lblSalarioLiquido.BackColor = System.Drawing.Color.Transparent;
            this.lblSalarioLiquido.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblSalarioLiquido.Location = new System.Drawing.Point(237, 366);
            this.lblSalarioLiquido.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSalarioLiquido.Name = "lblSalarioLiquido";
            this.lblSalarioLiquido.Size = new System.Drawing.Size(12, 16);
            this.lblSalarioLiquido.TabIndex = 11;
            this.lblSalarioLiquido.Text = "-";
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackColor = System.Drawing.Color.Transparent;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCalcular.Location = new System.Drawing.Point(110, 402);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(157, 36);
            this.btnCalcular.TabIndex = 8;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.Transparent;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSalvar.Location = new System.Drawing.Point(275, 402);
            this.btnSalvar.Margin = new System.Windows.Forms.Padding(4);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(142, 36);
            this.btnSalvar.TabIndex = 9;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // chkHoraExtra
            // 
            this.chkHoraExtra.AutoSize = true;
            this.chkHoraExtra.BackColor = System.Drawing.Color.Transparent;
            this.chkHoraExtra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkHoraExtra.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.chkHoraExtra.Location = new System.Drawing.Point(296, 236);
            this.chkHoraExtra.Margin = new System.Windows.Forms.Padding(4);
            this.chkHoraExtra.Name = "chkHoraExtra";
            this.chkHoraExtra.Size = new System.Drawing.Size(86, 20);
            this.chkHoraExtra.TabIndex = 14;
            this.chkHoraExtra.Text = "Hora extra";
            this.chkHoraExtra.UseVisualStyleBackColor = false;
            this.chkHoraExtra.CheckedChanged += new System.EventHandler(this.chkHoraExtra_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(41, 140);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 16);
            this.label2.TabIndex = 16;
            this.label2.Text = "Faltas na segunda semana :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(46, 110);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(173, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Faltas na primeira semana :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Location = new System.Drawing.Point(56, 199);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 16);
            this.label5.TabIndex = 18;
            this.label5.Text = "Faltas na quarta semana :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(49, 170);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(169, 16);
            this.label6.TabIndex = 17;
            this.label6.Text = "Faltas na terceira semana :";
            // 
            // txtFaltasPriSemana
            // 
            this.txtFaltasPriSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaltasPriSemana.Location = new System.Drawing.Point(227, 107);
            this.txtFaltasPriSemana.Margin = new System.Windows.Forms.Padding(4);
            this.txtFaltasPriSemana.Name = "txtFaltasPriSemana";
            this.txtFaltasPriSemana.Size = new System.Drawing.Size(190, 22);
            this.txtFaltasPriSemana.TabIndex = 2;
            // 
            // txtFaltaSegSemana
            // 
            this.txtFaltaSegSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaltaSegSemana.Location = new System.Drawing.Point(226, 137);
            this.txtFaltaSegSemana.Margin = new System.Windows.Forms.Padding(4);
            this.txtFaltaSegSemana.Name = "txtFaltaSegSemana";
            this.txtFaltaSegSemana.Size = new System.Drawing.Size(191, 22);
            this.txtFaltaSegSemana.TabIndex = 3;
            // 
            // txtFaltaQuarSemana
            // 
            this.txtFaltaQuarSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaltaQuarSemana.Location = new System.Drawing.Point(226, 197);
            this.txtFaltaQuarSemana.Margin = new System.Windows.Forms.Padding(4);
            this.txtFaltaQuarSemana.Name = "txtFaltaQuarSemana";
            this.txtFaltaQuarSemana.Size = new System.Drawing.Size(191, 22);
            this.txtFaltaQuarSemana.TabIndex = 5;
            // 
            // txtFaltaTerSemana
            // 
            this.txtFaltaTerSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaltaTerSemana.Location = new System.Drawing.Point(226, 167);
            this.txtFaltaTerSemana.Margin = new System.Windows.Forms.Padding(4);
            this.txtFaltaTerSemana.Name = "txtFaltaTerSemana";
            this.txtFaltaTerSemana.Size = new System.Drawing.Size(191, 22);
            this.txtFaltaTerSemana.TabIndex = 4;
            // 
            // FolhadePagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::mecanica.Properties.Resources.nmadeira2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(505, 469);
            this.Controls.Add(this.txtFaltaQuarSemana);
            this.Controls.Add(this.txtFaltaTerSemana);
            this.Controls.Add(this.txtFaltaSegSemana);
            this.Controls.Add(this.txtFaltasPriSemana);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chkHoraExtra);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.lblSalarioLiquido);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.lblPorcentagem);
            this.Controls.Add(this.nudPorcentagem);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudHorasTrabalhadas);
            this.Controls.Add(this.lblHoraExtra);
            this.Controls.Add(this.nudHorasExtras);
            this.Controls.Add(this.chkVT);
            this.Controls.Add(this.cboFuncionario);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FolhadePagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Folhade Pagamento";
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasExtras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasTrabalhadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPorcentagem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.CheckBox chkVT;
        private System.Windows.Forms.NumericUpDown nudHorasExtras;
        private System.Windows.Forms.Label lblHoraExtra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudHorasTrabalhadas;
        private System.Windows.Forms.Label lblPorcentagem;
        private System.Windows.Forms.NumericUpDown nudPorcentagem;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Label lblSalarioLiquido;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.CheckBox chkHoraExtra;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFaltasPriSemana;
        private System.Windows.Forms.TextBox txtFaltaSegSemana;
        private System.Windows.Forms.TextBox txtFaltaQuarSemana;
        private System.Windows.Forms.TextBox txtFaltaTerSemana;
    }
}