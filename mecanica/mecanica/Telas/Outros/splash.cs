﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Outros
{
    public partial class splash : Form
    {
        public splash()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(0000);

                Invoke(new Action(() =>
                {
                    while (valueCarregamento.Value < 100)
                    {
                        System.Threading.Thread.Sleep(0016);

                        valueCarregamento.Value = valueCarregamento.Value + 1;

                    }
                    // Abre a tela Inicial
                    frmLogin frm = new frmLogin();
                    frm.Show();
                    Hide();
                }));
            });

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
    }
}
