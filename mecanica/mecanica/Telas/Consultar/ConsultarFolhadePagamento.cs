﻿using Loja_de_roupas;
using mecanica.DB.Programação.Folha_de_Pagamento;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Consultar
{
    public partial class ConsultarFolhadePagamento : Form
    {
        public ConsultarFolhadePagamento()
        {
            InitializeComponent();
        }

        private void btnConsultarCliente_Click(object sender, EventArgs e)
        {
            FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
            List<FolhadePagamentoDTO> lista = business.Buscar(txtClienteConsultar.Text);

            dgvConsultarCliente.AutoGenerateColumns = false;
            dgvConsultarCliente.DataSource = lista;
        }

        private void dgvConsultarCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                FolhadePagamentoDTO dto = dgvConsultarCliente.CurrentRow.DataBoundItem as FolhadePagamentoDTO;
                business.Remover(dto.Id);
                btnConsultarCliente_Click(null, null);
            }
        }

        private void ConsultarFolhadePagamento_Load(object sender, EventArgs e)
        {

        }

        private void txtClienteConsultar_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtClienteConsultar_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }
    }
}
