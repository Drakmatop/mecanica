﻿using Loja_de_roupas;
using Loja_de_roupas.DB.Pedido;
using mecanica.DB.Funcionario;
using mecanica.DB.Programação.Pedido;
using mecanica.DB.Programação.PedidoServico;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Consultar
{
    public partial class Consultar_Pedido_Serviço : Form
    {
        public Consultar_Pedido_Serviço()
        {
            InitializeComponent();
        }

        private void btnConsultarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoBusiness pedido = new PedidoBusiness();
                List<VwPedidoServicoConsultar> view = pedido.ConsultarServico(txtCliente.Text);
                FuncionarioBusiness business = new FuncionarioBusiness();
                dgvServico.AutoGenerateColumns = false;
                dgvServico.DataSource = view;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");

            }
        }

        private void dgvServico_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                DialogResult r = MessageBox.Show("Deseja realmente excluir este item?", "Lottus Store", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    PedidoBusiness business = new PedidoBusiness();
                    PedidoServicoBusiness pedidoservico = new PedidoServicoBusiness();
                    VwPedidoServicoConsultar vw = dgvServico.CurrentRow.DataBoundItem as VwPedidoServicoConsultar;

                    pedidoservico.Remover(vw.Id);
                    business.Remover(vw.Id);
                    btnConsultarCliente_Click(null, null);
                }

            }
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }   
    }
}
