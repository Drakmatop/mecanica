﻿using Loja_de_roupas;
using mecanica.DB.Cliente;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica
{
    public partial class ConsultarCliente : Form
    {
        public ConsultarCliente()
        {
            InitializeComponent();
        }

        private void btnConsultarCliente_Click(object sender, EventArgs e)
        {
            try
            {

                ClienteBusiness business = new ClienteBusiness();
                List<ClienteDTO> dto = business.Buscar(txtClienteConsultar.Text);

                dgvConsultarCliente.AutoGenerateColumns = false;
                dgvConsultarCliente.DataSource = dto;

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
        }

        private void dgvConsultarCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                ClienteBusiness business = new ClienteBusiness();
                ClienteDTO dto = dgvConsultarCliente.CurrentRow.DataBoundItem as ClienteDTO;
                business.Remover(dto.Id);
                btnConsultarCliente_Click(null, null);
            }
        }

        private void txtClienteConsultar_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }
    }
}
