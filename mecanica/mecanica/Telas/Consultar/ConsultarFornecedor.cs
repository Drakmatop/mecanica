﻿using Loja_de_roupas;
using mecanica.DB.Programação.Fornecedor;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Consultar
{
    public partial class ConsultarFornecedor : Form
    {
        public ConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Buscar(txtFornecedor.Text);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void txtFornecedor_TextChanged(object sender, EventArgs e)
        {

        }

        private void ConsultarFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4)
                {
                    FornecedorBusiness business = new FornecedorBusiness();
                    FornecedorDTO dto = dataGridView1.CurrentRow.DataBoundItem as FornecedorDTO;
                    business.Remover(dto.Id);
                    button1_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possivel remover pois existe um produto vínculado a este fornecedor, por favor exclua o produto antes de tentar remover o forncedor", "Américas Mecânica");
            }
           
        }

        private void txtFornecedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }
    }
}
