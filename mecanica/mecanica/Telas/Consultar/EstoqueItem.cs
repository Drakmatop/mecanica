﻿using Loja_de_roupas.DB.Estoque;
using mecanica.DB.Produto;
using mecanica.DB.Programação.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica
{
    public partial class EstoqueItem : Form
    {
        public EstoqueItem()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Estoque_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            EstoqueBusiness business = new EstoqueBusiness();
            List<vwEstoque> vw = business.Consultar(txtConsultarEstoque.Text);

            dgvConsultarEstoque.AutoGenerateColumns = false;
            dgvConsultarEstoque.DataSource = vw;
        }

        private void dgvConsultarEstoque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                DialogResult r = MessageBox.Show("Deseja remover este item?", "Américas Mecânica", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    ProdutoBusiness produtoBusiness = new ProdutoBusiness();
                    vwEstoque vw = dgvConsultarEstoque.CurrentRow.DataBoundItem as vwEstoque;
                    EstoqueBusiness business = new EstoqueBusiness();
                    business.Remover(vw.Id);
                    produtoBusiness.Remover(vw.Id);
                    button1_Click(null, null);
                }
            }
        }
    }
}
