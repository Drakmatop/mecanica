﻿using Loja_de_roupas.DB.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Consultar
{
    public partial class ConsultarGastos : Form
    {
        public ConsultarGastos()
        {
            InitializeComponent();
        }

        private void btnConsultarFuncionario_Click(object sender, EventArgs e)
        {
            GastoBusiness business = new GastoBusiness();
            List<GastoDTO> lista = business.Consultar(txtGasto.Text);

            dgvConsultarFuncionario.AutoGenerateColumns = false;
            dgvConsultarFuncionario.DataSource = lista;
        }

        private void dgvConsultarFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                DialogResult r = MessageBox.Show("Deseja remover este item?", "Américas Mecânica", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    GastoDTO vw = dgvConsultarFuncionario.CurrentRow.DataBoundItem as GastoDTO;
                    GastoBusiness business = new GastoBusiness();
                    business.Remover(vw.Id);
                }
            }
        }

        
    }
}
