﻿using Loja_de_roupas;
using Loja_de_roupas.DB.Pedido;
using mecanica.DB.Funcionario;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Consultar
{
    public partial class ConsultarPedido : Form
    {
        public ConsultarPedido()
        {
            InitializeComponent();
        }

        private void btnConsultarPedido_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoBusiness pedido = new PedidoBusiness();
                List<PedidoConsultarView> view = pedido.Consultar(txtCliente.Text);
                FuncionarioBusiness business = new FuncionarioBusiness();
                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = view;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void dgvConsultarPedido_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                DialogResult r = MessageBox.Show("Deseja realmente excluir este item?", "Lottus Store", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    PedidoBusiness business = new PedidoBusiness();
                    PedidoItemBusiness pedidoitem = new PedidoItemBusiness();
                    PedidoConsultarView vw = dataGridView1.CurrentRow.DataBoundItem as PedidoConsultarView;

                    pedidoitem.Remover(vw.Id);
                    business.Remover(vw.Id);
                    btnConsultarPedido_Click(null, null);
                }

            }
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }
    }
}
