﻿using Loja_de_roupas;
using mecanica.DB.Funcionario;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica
{
    public partial class ConsultarFuncionario : Form
    {
        public ConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 9)
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO dto = dgvConsultarFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;
                business.Remover(dto.Id);
                btnConsultarFuncionario_Click(null, null);
            }


        }

        private void btnConsultarFuncionario_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                List<FuncionarioDTO> dto = business.Consultar(txtConsultarFuncionario.Text);

                dgvConsultarFuncionario.AutoGenerateColumns = false;
                dgvConsultarFuncionario.DataSource = dto;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
        }

        private void txtConsultarFuncionario_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtConsultarFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }
    }
}
