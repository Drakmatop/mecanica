﻿using Loja_de_roupas;
using mecanica.DB.Programação.Categoria;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Consultar
{
    public partial class Consultar_Categoria : Form
    {
        public Consultar_Categoria()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CategoriaBusiness business = new CategoriaBusiness();
            List<CategoriaDTO> dto = business.Buscar(txtCategoria.Text);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = dto;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                CategoriaBusiness business = new CategoriaBusiness();
                CategoriaDTO dto = dataGridView1.CurrentRow.DataBoundItem as CategoriaDTO;
                business.Remover(dto.ID);
                button1_Click(null, null);
            }
        }

        private void txtCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }
    }
}
