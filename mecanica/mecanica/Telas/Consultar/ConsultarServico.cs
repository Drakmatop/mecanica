﻿using Loja_de_roupas;
using mecanica.DB.Serviço;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Consultar
{
    public partial class ConsultarServico : Form
    {
        public ConsultarServico()
        {
            InitializeComponent();
        }

        private void btnConsultarCliente_Click(object sender, EventArgs e)
        {
            ServiçoBusiness business = new ServiçoBusiness();
            List<ServiçoDTO> dto = business.Buscar(txtServico.Text);

            dgvConsultarCliente.AutoGenerateColumns = false;
            dgvConsultarCliente.DataSource = dto;

        }

        private void dgvConsultarCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                ServiçoBusiness business = new ServiçoBusiness();
                ServiçoDTO dto = dgvConsultarCliente.CurrentRow.DataBoundItem as ServiçoDTO;
                business.Remover(dto.Id);
                btnConsultarCliente_Click(null, null);
            }
        }

        private void ConsultarServico_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtServico_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }
    }
}
