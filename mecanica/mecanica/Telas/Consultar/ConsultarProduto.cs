﻿using Loja_de_roupas;
using mecanica.DB.Produto;
using Michaelpops.Programação;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Consultar
{
    public partial class ConsultarProduto : Form
    {
        public ConsultarProduto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Buscar(txtProduto.Text);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                ProdutoBusiness business = new ProdutoBusiness();
                ProdutoDTO dto = dataGridView1.CurrentRow.DataBoundItem as ProdutoDTO;

                business.Remover(dto.Id);
                button1_Click(null, null);
            }
        }

        private void txtProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.letras(e);
        }

        private void btnConsultarPedido_Click(object sender, EventArgs e)
        {

        }
    }
}
