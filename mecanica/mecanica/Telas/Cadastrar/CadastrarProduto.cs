﻿using Loja_de_roupas.DB.Estoque;
using mecanica.DB.Produto;
using mecanica.DB.Programação.Categoria;
using mecanica.DB.Programação.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Cadastrar
{
    public partial class CadastrarProduto : Form
    {
        public CadastrarProduto()
        {
            InitializeComponent();
            CarregarCombo();
        }
        private void CarregarCombo()
        {
            CategoriaBusiness business = new CategoriaBusiness();
            List<CategoriaDTO> lista = business.Listar();
            cboCategoria.DisplayMember = nameof(CategoriaDTO.Nome);
            cboCategoria.ValueMember = nameof(CategoriaDTO.ID);
            cboCategoria.DataSource = lista;

            FornecedorBusiness fornecedor = new FornecedorBusiness();
            List<FornecedorDTO> listaa = fornecedor.Listar();
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFornecedor .ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DataSource = listaa;
        }



        private void btnCadastrar_Click(object sender, EventArgs e)
        {
           
            try
            {
                ProdutoBusiness business = new ProdutoBusiness();
                CategoriaDTO categoria = cboCategoria.SelectedItem as CategoriaDTO;
                FornecedorDTO fornecedor = cboFornecedor.SelectedItem as FornecedorDTO;
                EstoqueDTO estoque = new EstoqueDTO();
                EstoqueBusiness estoquebusiness = new EstoqueBusiness();

                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = txtNome.Text;
                dto.Marca = txtMarca.Text;
                dto.IdCategoria = categoria.ID;
                dto.PreçoCompra = nudPrecoCompra.Value;
                dto.PrecoVenda = nudPrecodeVenda.Value;
                dto.IdFornecedor = fornecedor.Id;
                int idproduto = business.Salvar(dto);
                estoque.IdProduto = idproduto;
                estoque.Quantidade = 0;
                estoquebusiness.Salvar(estoque);

                MessageBox.Show("Produto cadastrado com sucesso", "Américas Mecânica");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
            
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void nudPrecodeVenda_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtMarca_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void nudPrecoCompra_ValueChanged(object sender, EventArgs e)
        {

        }

        private void cboCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cbofornecedor_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }
    }
}
