﻿using mecanica.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica
{
    public partial class CadastrarCliente : Form
    {
        public CadastrarCliente()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Nome = txtNome.Text;
                dto.Sobrenome = txtSobrenome.Text;
                dto.Nascimento = dtpNascimento.Value;
                dto.RG = txtRG.Text;
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Cep = txtCep.Text;
                dto.Genero = cboGenero.Text;

                ClienteBusiness business = new ClienteBusiness();
                business.Salvar(dto);
                MessageBox.Show("Cliente salvo com sucesso", "America Mecanica");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
            


        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Telas.frmMenu tela = new Telas.frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
