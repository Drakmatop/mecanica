﻿namespace mecanica
{
    partial class CadastrarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtNomeFuncionario = new System.Windows.Forms.TextBox();
            this.btnCadastrarFuncionario = new System.Windows.Forms.Button();
            this.txtSobrenome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSenhaFuncionario = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUsuarioFuncionario = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtComplementoFuncionario = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTelFuncionario = new System.Windows.Forms.MaskedTextBox();
            this.txtEmailFuncionario = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtRGFuncionario = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.rdnADM = new System.Windows.Forms.RadioButton();
            this.rdnVendedor = new System.Windows.Forms.RadioButton();
            this.rdnMecanico = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.txtConfirmarSenhaFuncionario = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chkPlanodeSaudeFuncionario = new System.Windows.Forms.CheckBox();
            this.nudSalarioB = new System.Windows.Forms.NumericUpDown();
            this.txtCpf = new System.Windows.Forms.MaskedTextBox();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalarioB)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(31, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome :";
            // 
            // TxtNomeFuncionario
            // 
            this.TxtNomeFuncionario.BackColor = System.Drawing.Color.White;
            this.TxtNomeFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNomeFuncionario.Location = new System.Drawing.Point(88, 27);
            this.TxtNomeFuncionario.Name = "TxtNomeFuncionario";
            this.TxtNomeFuncionario.Size = new System.Drawing.Size(196, 22);
            this.TxtNomeFuncionario.TabIndex = 0;
            // 
            // btnCadastrarFuncionario
            // 
            this.btnCadastrarFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.btnCadastrarFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrarFuncionario.ForeColor = System.Drawing.Color.White;
            this.btnCadastrarFuncionario.Location = new System.Drawing.Point(503, 197);
            this.btnCadastrarFuncionario.Name = "btnCadastrarFuncionario";
            this.btnCadastrarFuncionario.Size = new System.Drawing.Size(108, 68);
            this.btnCadastrarFuncionario.TabIndex = 17;
            this.btnCadastrarFuncionario.Text = "Cadastrar";
            this.btnCadastrarFuncionario.UseVisualStyleBackColor = false;
            this.btnCadastrarFuncionario.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSobrenome
            // 
            this.txtSobrenome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSobrenome.Location = new System.Drawing.Point(394, 26);
            this.txtSobrenome.Name = "txtSobrenome";
            this.txtSobrenome.Size = new System.Drawing.Size(218, 22);
            this.txtSobrenome.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(302, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Sobrenome :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(41, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "CEP :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(42, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "CPF :";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(301, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Nascimento :";
            // 
            // txtSenhaFuncionario
            // 
            this.txtSenhaFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSenhaFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenhaFuncionario.Location = new System.Drawing.Point(88, 194);
            this.txtSenhaFuncionario.Name = "txtSenhaFuncionario";
            this.txtSenhaFuncionario.Size = new System.Drawing.Size(196, 22);
            this.txtSenhaFuncionario.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(29, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "Senha :";
            // 
            // txtUsuarioFuncionario
            // 
            this.txtUsuarioFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuarioFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuarioFuncionario.Location = new System.Drawing.Point(88, 166);
            this.txtUsuarioFuncionario.Name = "txtUsuarioFuncionario";
            this.txtUsuarioFuncionario.Size = new System.Drawing.Size(196, 22);
            this.txtUsuarioFuncionario.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(21, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "Usuario :";
            // 
            // txtComplementoFuncionario
            // 
            this.txtComplementoFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComplementoFuncionario.Location = new System.Drawing.Point(394, 82);
            this.txtComplementoFuncionario.Name = "txtComplementoFuncionario";
            this.txtComplementoFuncionario.Size = new System.Drawing.Size(218, 22);
            this.txtComplementoFuncionario.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(289, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "Complemento :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(48, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 16);
            this.label9.TabIndex = 17;
            this.label9.Text = "Tel :";
            // 
            // txtTelFuncionario
            // 
            this.txtTelFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelFuncionario.Location = new System.Drawing.Point(88, 110);
            this.txtTelFuncionario.Mask = "(99) 00000-0000";
            this.txtTelFuncionario.Name = "txtTelFuncionario";
            this.txtTelFuncionario.Size = new System.Drawing.Size(100, 22);
            this.txtTelFuncionario.TabIndex = 3;
            // 
            // txtEmailFuncionario
            // 
            this.txtEmailFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmailFuncionario.Location = new System.Drawing.Point(394, 110);
            this.txtEmailFuncionario.Name = "txtEmailFuncionario";
            this.txtEmailFuncionario.Size = new System.Drawing.Size(218, 22);
            this.txtEmailFuncionario.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(339, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 16);
            this.label10.TabIndex = 19;
            this.label10.Text = "Email :";
            // 
            // txtRGFuncionario
            // 
            this.txtRGFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRGFuncionario.Location = new System.Drawing.Point(88, 138);
            this.txtRGFuncionario.Mask = "00,000,000-0";
            this.txtRGFuncionario.Name = "txtRGFuncionario";
            this.txtRGFuncionario.Size = new System.Drawing.Size(100, 22);
            this.txtRGFuncionario.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(48, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "RG :";
            // 
            // rdnADM
            // 
            this.rdnADM.AutoSize = true;
            this.rdnADM.BackColor = System.Drawing.Color.Transparent;
            this.rdnADM.ForeColor = System.Drawing.Color.White;
            this.rdnADM.Location = new System.Drawing.Point(394, 192);
            this.rdnADM.Name = "rdnADM";
            this.rdnADM.Size = new System.Drawing.Size(54, 20);
            this.rdnADM.TabIndex = 14;
            this.rdnADM.TabStop = true;
            this.rdnADM.Text = "Adm";
            this.rdnADM.UseVisualStyleBackColor = false;
            // 
            // rdnVendedor
            // 
            this.rdnVendedor.AutoSize = true;
            this.rdnVendedor.BackColor = System.Drawing.Color.Transparent;
            this.rdnVendedor.ForeColor = System.Drawing.Color.White;
            this.rdnVendedor.Location = new System.Drawing.Point(394, 218);
            this.rdnVendedor.Name = "rdnVendedor";
            this.rdnVendedor.Size = new System.Drawing.Size(86, 20);
            this.rdnVendedor.TabIndex = 15;
            this.rdnVendedor.TabStop = true;
            this.rdnVendedor.Text = "Vendedor";
            this.rdnVendedor.UseVisualStyleBackColor = false;
            // 
            // rdnMecanico
            // 
            this.rdnMecanico.AutoSize = true;
            this.rdnMecanico.BackColor = System.Drawing.Color.Transparent;
            this.rdnMecanico.ForeColor = System.Drawing.Color.White;
            this.rdnMecanico.Location = new System.Drawing.Point(394, 242);
            this.rdnMecanico.Name = "rdnMecanico";
            this.rdnMecanico.Size = new System.Drawing.Size(85, 20);
            this.rdnMecanico.TabIndex = 16;
            this.rdnMecanico.TabStop = true;
            this.rdnMecanico.Text = "Mecanico";
            this.rdnMecanico.UseVisualStyleBackColor = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(318, 141);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 16);
            this.label13.TabIndex = 28;
            this.label13.Text = "Salario B :";
            // 
            // txtConfirmarSenhaFuncionario
            // 
            this.txtConfirmarSenhaFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfirmarSenhaFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirmarSenhaFuncionario.Location = new System.Drawing.Point(88, 242);
            this.txtConfirmarSenhaFuncionario.Name = "txtConfirmarSenhaFuncionario";
            this.txtConfirmarSenhaFuncionario.Size = new System.Drawing.Size(196, 22);
            this.txtConfirmarSenhaFuncionario.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(85, 223);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 16);
            this.label12.TabIndex = 32;
            this.label12.Text = "Confirmar Senha :";
            // 
            // chkPlanodeSaudeFuncionario
            // 
            this.chkPlanodeSaudeFuncionario.AutoSize = true;
            this.chkPlanodeSaudeFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.chkPlanodeSaudeFuncionario.ForeColor = System.Drawing.Color.Transparent;
            this.chkPlanodeSaudeFuncionario.Location = new System.Drawing.Point(394, 168);
            this.chkPlanodeSaudeFuncionario.Name = "chkPlanodeSaudeFuncionario";
            this.chkPlanodeSaudeFuncionario.Size = new System.Drawing.Size(124, 20);
            this.chkPlanodeSaudeFuncionario.TabIndex = 13;
            this.chkPlanodeSaudeFuncionario.Text = "Plano de Saúde";
            this.chkPlanodeSaudeFuncionario.UseVisualStyleBackColor = false;
            // 
            // nudSalarioB
            // 
            this.nudSalarioB.DecimalPlaces = 2;
            this.nudSalarioB.Location = new System.Drawing.Point(394, 136);
            this.nudSalarioB.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudSalarioB.Name = "nudSalarioB";
            this.nudSalarioB.Size = new System.Drawing.Size(217, 22);
            this.nudSalarioB.TabIndex = 9;
            // 
            // txtCpf
            // 
            this.txtCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCpf.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCpf.Location = new System.Drawing.Point(88, 82);
            this.txtCpf.Mask = "000,000,000-00";
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(122, 20);
            this.txtCpf.TabIndex = 2;
            // 
            // txtCep
            // 
            this.txtCep.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCep.Location = new System.Drawing.Point(88, 56);
            this.txtCep.Mask = "00000-000";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(122, 20);
            this.txtCep.TabIndex = 1;
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Location = new System.Drawing.Point(394, 54);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(217, 22);
            this.dtpNascimento.TabIndex = 6;
            // 
            // CadastrarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::mecanica.Properties.Resources.nmadeira2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(647, 289);
            this.Controls.Add(this.dtpNascimento);
            this.Controls.Add(this.txtCep);
            this.Controls.Add(this.txtCpf);
            this.Controls.Add(this.nudSalarioB);
            this.Controls.Add(this.chkPlanodeSaudeFuncionario);
            this.Controls.Add(this.txtConfirmarSenhaFuncionario);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.rdnMecanico);
            this.Controls.Add(this.rdnVendedor);
            this.Controls.Add(this.rdnADM);
            this.Controls.Add(this.txtRGFuncionario);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtEmailFuncionario);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtTelFuncionario);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtComplementoFuncionario);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtSenhaFuncionario);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtUsuarioFuncionario);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSobrenome);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCadastrarFuncionario);
            this.Controls.Add(this.TxtNomeFuncionario);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CadastrarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar Funcionario";
            this.Load += new System.EventHandler(this.CadastrarFuncionario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudSalarioB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtNomeFuncionario;
        private System.Windows.Forms.Button btnCadastrarFuncionario;
        private System.Windows.Forms.TextBox txtSobrenome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSenhaFuncionario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUsuarioFuncionario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtComplementoFuncionario;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox txtTelFuncionario;
        private System.Windows.Forms.TextBox txtEmailFuncionario;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txtRGFuncionario;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton rdnADM;
        private System.Windows.Forms.RadioButton rdnVendedor;
        private System.Windows.Forms.RadioButton rdnMecanico;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtConfirmarSenhaFuncionario;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkPlanodeSaudeFuncionario;
        private System.Windows.Forms.NumericUpDown nudSalarioB;
        private System.Windows.Forms.MaskedTextBox txtCpf;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
    }
}