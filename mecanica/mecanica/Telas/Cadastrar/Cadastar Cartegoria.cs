﻿using mecanica.DB.Programação.Categoria;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Cadastrar
{
    public partial class Cadastar_Cartegoria : Form
    {
        public Cadastar_Cartegoria()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CategoriaDTO dto = new CategoriaDTO();
                dto.Nome = txtCategoria.Text;
                CategoriaBusiness business = new CategoriaBusiness();
                business.Salvar(dto);
                MessageBox.Show("Categoria salva com sucesso", "Américas Mecânica");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
            
        }
    }
}
