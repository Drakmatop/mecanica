﻿using Loja_de_roupas.DB.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Cadastrar
{
    public partial class CadastrarGastos : Form
    {
        public CadastrarGastos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                GastoDTO dto = new GastoDTO();

                dto.Gasto = txtGasto.Text;
                dto.Tipo = cboTipo.SelectedItem.ToString();
                dto.Pagamento = dtpPagamento.Value;
                dto.Valor = nudValor.Value;

                GastoBusiness business = new GastoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Gasto salvo com sucesso", "Américas Mecânica");
                txtGasto.Clear();
                nudValor.Value = 0.00m;
                dtpPagamento.Value = DateTime.Now;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Américas Mecânica");
            }
        }
    }
}
