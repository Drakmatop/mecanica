﻿using mecanica.DB.Serviço;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Cadastrar
{
    public partial class CadastrarServico : Form
    {
        public CadastrarServico()
        {
            InitializeComponent();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                ServiçoDTO dto = new ServiçoDTO();
                dto.Nome = txtServico.Text;
                dto.Preco = nudPreco.Value;
                dto.Descricao = txtDescricao.Text;

                ServiçoBusiness business = new ServiçoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Cadastro efetuado com sucesso", "Américas Mecânica");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Américas Mecânica");
            }
          
        }
    }
}
