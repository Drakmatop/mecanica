﻿using Loja_de_roupas.DB.Estoque;
using Loja_de_roupas.DB.Pedido;
using mecanica.DB.Cliente;
using mecanica.DB.Produto;
using mecanica.DB.Programação.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica
{
    public partial class Pedido : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        public Pedido()
        {
            InitializeComponent();
            CarregarCombos();
        }
        decimal valordavenda = 0;

        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Buscar(string.Empty);
            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = lista;

            ClienteBusiness clientebus = new ClienteBusiness();
            List<ClienteDTO> clielista = clientebus.Listar();
            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
            cboCliente.DataSource = clielista;
        }
        int quantidade = 0;
        int i = 10;
        private void btnEmitir_Click(object sender, EventArgs e)
        {
            try
            {
                EstoqueBusiness estoquebussiness = new EstoqueBusiness();

                List<EstoqueDTO> estoquedto = estoquebussiness.Listar();


                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
                int qtd = Convert.ToInt32(nudQuantidade.Value);


                foreach (EstoqueDTO item in estoquedto)
                {
                    if (dto.Id == item.IdProduto)
                    {
                        if (i == 0)
                        {
                            if (quantidade < qtd)
                            {
                                MessageBox.Show("Quantidade de " + dto.Nome + " Não é suficiente para atender a esta venda, atualmente temos em estoque " + quantidade + " " + dto.Nome);
                            }
                            else
                            {

                                for (i = 0; i < qtd; i++)

                                {
                                    produtosCarrinho.Add(dto);
                                    valordavenda = valordavenda + dto.PrecoVenda;
                                    lblValordaVenda.Text = valordavenda.ToString();

                                }

                                quantidade = item.Quantidade - qtd;

                                dgvProduto.AutoGenerateColumns = false;
                                dgvProduto.DataSource = produtosCarrinho;
                                i = 0;
                            }
                        }
                        else
                        {
                            if (item.Quantidade < qtd)
                            {
                                MessageBox.Show("Quantidade de " + dto.Nome + " Não é suficiente para atender a esta venda, atualmente temos em estoque " + item.Quantidade + " " + dto.Nome);
                            }
                            else
                            {

                                for (i = 0; i < qtd; i++)

                                {
                                    produtosCarrinho.Add(dto);
                                    valordavenda = valordavenda + dto.PrecoVenda;
                                    lblValordaVenda.Text = valordavenda.ToString();

                                }

                                quantidade = item.Quantidade - qtd;

                                dgvProduto.AutoGenerateColumns = false;
                                dgvProduto.DataSource = produtosCarrinho;
                                i = 0;
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            produtosCarrinho = new BindingList<ProdutoDTO>();
            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = produtosCarrinho;
            valordavenda = 0;
            lblValordaVenda.Text = valordavenda.ToString();
            quantidade = 0;
            i = 10;
        }

        private void btnEmitir_Click_1(object sender, EventArgs e)
        {
            try
            {

                ClienteDTO clie = cboCliente.SelectedItem as ClienteDTO;
                ProdutoDTO produto = cboProduto.SelectedItem as ProdutoDTO;

                PedidoItemDTO pedidoitem = new PedidoItemDTO();
                PedidoDTO dto = new PedidoDTO();
                dto.FormaPagamento = cboFormadePagamento.Text;
                dto.Data = DateTime.Now;
                dto.ClienteId = clie.Id;
                dto.FuncionarioId = UserSession.UsuarioLogado.Id;
                pedidoitem.IdProduto = produto.Id;
                PedidoBusiness business = new PedidoBusiness();
                int idpedido = business.Salvar(dto, produtosCarrinho.ToList());



                EstoqueBusiness businessestoque = new EstoqueBusiness();
                List<PedidoConsultarView> lista = business.ConsultarPorId(idpedido);
                List<EstoqueDTO> estoque = businessestoque.Listar();

                foreach (PedidoConsultarView item in lista)
                {
                    foreach (EstoqueDTO item2 in estoque)
                    {
                        if (item.IdProduto == item2.IdProduto)
                        {
                            item2.Quantidade = item2.Quantidade - item.QtdItens;
                        }
                    }
                }


                foreach (EstoqueDTO item in estoque)
                {
                    EstoqueDTO estoquedto = new EstoqueDTO();

                    estoquedto.IdProduto = item.IdProduto;
                    estoquedto.Quantidade = item.Quantidade;

                    businessestoque.Alterar(estoquedto);
                }

                MessageBox.Show("Pedido salvo com sucesso.", "Lottus Store", MessageBoxButtons.OK, MessageBoxIcon.Information);
                produtosCarrinho = new BindingList<ProdutoDTO>();
            }

            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Américas Mecânica");
            }

        }
    }
}


