﻿using Loja_de_roupas.DB.Pedido;
using mecanica.DB.Cliente;
using mecanica.DB.Programação.Funcionario;
using mecanica.DB.Programação.PedidoServico;
using mecanica.DB.Serviço;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica.Telas.Cadastrar
{
    public partial class Pedido_Serviço : Form
    {
        public Pedido_Serviço()
        {
            InitializeComponent();
            CarregarCombos();
        }
        BindingList<ServiçoDTO> produtosCarrinho = new BindingList<ServiçoDTO>();
       
        decimal valordavenda = 0;

        void CarregarCombos()
        {
            ServiçoBusiness business = new ServiçoBusiness();
            List<ServiçoDTO> lista = business.Buscar(string.Empty);
            cboServico.ValueMember = nameof(ServiçoDTO.Id);
            cboServico.DisplayMember = nameof(ServiçoDTO.Nome);
            cboServico.DataSource = lista;

            ClienteBusiness clientebus = new ClienteBusiness();
            List<ClienteDTO> clielista = clientebus.Listar();
            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
            cboCliente.DataSource = clielista;
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            produtosCarrinho = new BindingList<ServiçoDTO>();
            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = produtosCarrinho;
            valordavenda = 0;
            lblValordaVenda.Text = valordavenda.ToString();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                ServiçoDTO dto = cboServico.SelectedItem as ServiçoDTO;
                int qtd = Convert.ToInt32(nudQuantidade.Value);

                for (int i = 0; i < qtd; i++)

                {
                    produtosCarrinho.Add(dto);
                    valordavenda = valordavenda + dto.Preco;
                    lblValordaVenda.Text = valordavenda.ToString();

                }

                dgvProduto.AutoGenerateColumns = false;
                dgvProduto.DataSource = produtosCarrinho;
            }
            catch (Exception ex)
            {

                MessageBox.Show("ocorreu o erro" + ex.Message, "Américas Mecânica");
            }
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(nudQuantidade.Value) == 0)
                {
                    MessageBox.Show("Quantidae é obrigatório");
                }
                else
                {

                    ClienteDTO clie = cboCliente.SelectedItem as ClienteDTO;
                    ServiçoDTO servico = cboServico.SelectedItem as ServiçoDTO;

                    PedidoServicoDTO servicoitem = new PedidoServicoDTO();
                    PedidoDTO dto = new PedidoDTO();
                    dto.FormaPagamento = cboFormadePagamento.Text;
                    dto.Data = DateTime.Now;
                    dto.ClienteId = clie.Id;
                    dto.FuncionarioId = UserSession.UsuarioLogado.Id;
                    servicoitem.Idserviço = servico.Id;

                    PedidoBusiness business = new PedidoBusiness();

                    business.SalvarServico(dto, produtosCarrinho.ToList());


                    MessageBox.Show("Pedido salvo com sucesso.", "Américas Mecânica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    produtosCarrinho = new BindingList<ServiçoDTO>();
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "América Mecânica");
            }
        }
    }
}
