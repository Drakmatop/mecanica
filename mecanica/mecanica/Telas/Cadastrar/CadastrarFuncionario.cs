﻿using mecanica.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica
{
    public partial class CadastrarFuncionario : Form
    {
        public CadastrarFuncionario()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void CadastrarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtSenhaFuncionario.Text == txtConfirmarSenhaFuncionario.Text)
            {
                try
                {
                    FuncionarioDTO dto = new FuncionarioDTO();
                    dto.Nome = TxtNomeFuncionario.Text;
                    dto.Sobrenome = txtSobrenome.Text;
                    dto.RG = txtRGFuncionario.Text;
                    dto.Cpf = txtCpf.Text;
                    dto.Complemento = txtComplementoFuncionario.Text;
                    dto.Email = txtEmailFuncionario.Text;
                    dto.Login = txtUsuarioFuncionario.Text;
                    dto.Senha = txtSenhaFuncionario.Text;
                    dto.Telefone = txtTelFuncionario.Text;
                    dto.SalarioBruto = nudSalarioB.Value;
                    dto.PermissaoADM = rdnADM.Checked;
                    dto.PermissaoMecanico = rdnMecanico.Checked;
                    dto.PermissaoVENDEDOR = rdnVendedor.Checked;
                    dto.PlanodeSaude = chkPlanodeSaudeFuncionario.Checked;
                    dto.Cep = txtCep.Text;
                    dto.Nascimento = dtpNascimento.Value;

                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Salvar(dto);
                    MessageBox.Show("Funcionario salvo com sucesso", "America Mecanica");

                }
                catch (Exception ex)
                {

                    MessageBox.Show("Ocorreu o erro : " + ex.Message);
                }
               
            }
            else
            {
                MessageBox.Show("Senhas não são iguais", "American Mecanica");
            }
        }
    }
}
