﻿using mecanica.DB.Programação.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mecanica
{
    public partial class CadastrarFornecedor : Form
    {
        public CadastrarFornecedor()
        {
            InitializeComponent();
        }

        private void Fornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedorDTO dto = new FornecedorDTO();
            dto.Nome = txtNome.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Email = txtEmail.Text;
            dto.Cep = txtCep.Text;

            FornecedorBusiness business = new FornecedorBusiness();
            business.Salvar(dto);
            MessageBox.Show("Cadastro efetuado com sucesso", "Américas Mecânica");



        }
    }
}
